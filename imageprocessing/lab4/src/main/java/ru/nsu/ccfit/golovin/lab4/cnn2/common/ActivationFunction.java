package ru.nsu.ccfit.golovin.lab4.cnn2.common;

import java.util.function.DoubleUnaryOperator;

import static java.lang.Math.exp;
import static java.lang.Math.pow;

public enum ActivationFunction {
	RELU(x -> (x > 0.) ? 1. * x : pow(10, -6) * x, x -> (x > 0.) ? 1. : pow(10, -6)),
	SIGMOID(x -> 1. / (1. + exp(-x)), x -> (x * (1. - x)));

	private final DoubleUnaryOperator activation;
	private final DoubleUnaryOperator derivation;
	
    ActivationFunction(DoubleUnaryOperator activation, DoubleUnaryOperator derivation) {
    	this.activation = activation;
    	this.derivation = derivation;
    }

    public double activate(double x) { return activation.applyAsDouble(x); }
	
	public double derivative(double x) { return derivation.applyAsDouble(x); }
}
