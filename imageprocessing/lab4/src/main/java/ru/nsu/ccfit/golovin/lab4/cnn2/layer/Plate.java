package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import ru.nsu.ccfit.golovin.lab4.cnn2.common.ActivationFunction;

import java.util.Objects;

import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.checkPositive;
import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.checkValueInRange;

public class Plate {
    private double[][] values;
    private double[][] result;

    Plate(double[][] values) {
        Objects.requireNonNull(values, "Plate values");
        checkPositive(values.length, "Plate height");
        checkPositive(values[0].length, "Plate width");
        this.values = values;
    }

    int getHeight() { return values.length; }

    int getWidth() { return values[0].length; }

    int getTotalNumValues() { return getHeight() * getWidth(); }

    double[][] getValues() {
        return values;
    }

    double valueAt( int row, int col) {
        checkValueInRange(row, 0, getHeight(), "Row index");
        checkValueInRange(col, 0, getWidth(), "Column index");
        return values[row][col];
    }

    Plate convolve(Plate mask, boolean[][] activeMatrix) {
        checkValidMask(mask);
        int maskHeight = mask.getHeight();
        int maskWidth = mask.getWidth();
        if (result == null) {
            result = new double[getHeight() - maskHeight + 1][getWidth() - maskWidth + 1];
        }
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = convolvePixelIJ(mask, activeMatrix, i + maskHeight / 2, j + maskWidth / 2);
            }
        }
        return new Plate(result);
    }

    private double convolvePixelIJ(Plate mask, boolean[][] activeMatrix, int i, int j) {
        double sum = 0.0;
        for (int k = 0; k < mask.getHeight(); k++) {
            for (int l = 0; l < mask.getWidth(); l++) {
                int neighborX = i - (mask.getHeight() / 2) + k;
                int neighborY = j - (mask.getWidth() / 2) + l;
                if (neighborX < 0 || neighborX >= getHeight()
                        || neighborY < 0 || neighborY >= getWidth()
                        || !activeMatrix[k][l]) {
                    continue;
                }
                sum += mask.values[k][l] * values[neighborX][neighborY];
            }
        }
        return sum;
    }

    private void checkValidMask(Plate mask) {
        if (getHeight() < mask.getHeight() || getWidth() < mask.getWidth()) {
            throw new IllegalArgumentException("Mask must be smaller than plate.");
        }
    }

    Plate rot180() {
        double[][] result = new double[getHeight()][getWidth()];
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                result[i][j] = values[getHeight()-1-i][getWidth()-1-j];
            }
        }
        return new Plate(result);
    }

    void applyActivation(ActivationFunction func) {
        Objects.requireNonNull(func, "Activation function");
        double[][] output = new double[getHeight()][getWidth()];
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                output[i][j] = func.activate(values[i][j]);
            }
        }
        values = output;
    }

    double[] flatten() {
        double[] result = new double[getTotalNumValues()];
        for (int row = 0; row < getHeight(); row++) {
            System.arraycopy(
                    values[row],
                    0,
                    result,
                    row * getWidth(),
                    getWidth());
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(
                String.format(
                        "Plate with dimensions %dx%d:\n",
                        getHeight(),
                        getWidth()));
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                builder.append(String.format("%f, ", values[i][j]));
            }
            builder.append("\n");
        }
        builder.append("]\n");
        return builder.toString();
    }
}
