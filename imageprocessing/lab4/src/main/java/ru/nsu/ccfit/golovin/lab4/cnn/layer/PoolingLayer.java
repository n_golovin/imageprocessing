package ru.nsu.ccfit.golovin.lab4.cnn.layer;

import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;

import static java.lang.Double.max;

public class PoolingLayer extends Layer {
    private final int stride;
    private final int filterSize;

    public PoolingLayer(final Tensor input, final Tensor outputGradients, final double learningRate, final int stride, final int filterSize) {
        super(input, new Tensor(
                        (input.getDimensions().x - filterSize) / stride + 1,
                        (input.getDimensions().y - filterSize) / stride + 1,
                        input.getDimensions().z),
                new Tensor((input.getDimensions().x - filterSize) / stride + 1,
                        (input.getDimensions().y - filterSize) / stride + 1,
                        input.getDimensions().z), outputGradients, learningRate);
        this.stride = stride;
        this.filterSize = filterSize;
    }

    @Override
    public void forward() {
        output.applyFunction((x, y, z) -> {
            double pooledValue = -Double.MAX_VALUE;
            for (int i = 0; i < filterSize; i++) {
                for (int j = 0; j < filterSize; j++) {
                    pooledValue = max(pooledValue, input.getValue(x * stride + i, y * stride + j, z));
                }
            }
            return pooledValue;
        });
//        System.out.println("Output: " + output);
    }

    @Override
    public void backward() {
        inputGradients.applyFunction((x, y, z) -> {
            filterLoop: for (int i = 0; i < filterSize; i++) {
                for (int j = 0; j < filterSize; j++) {
                    if (Double.compare(output.getValue(x, y, z), input.getValue(x * stride + i, y * stride + j, z)) == 0) {
                        outputGradients.setValue(x * stride + i, y * stride + j, z, inputGradients.getValue(x, y, z));
                        break filterLoop;
                    }
                }
            }
            return inputGradients.getValue(x, y, z);
        });
//        System.out.println("Input gradients: " + inputGradients);
    }
}
