package ru.nsu.ccfit.golovin.lab4.cnn.util;

import lombok.NonNull;

import java.util.Arrays;

public class Tensor {
    private final double[][][] data;
    private final Dimensions dimensions;

    public Tensor(final int x, final int y, final int z) {
        this.data = new double[x][y][z];
        this.dimensions = new Dimensions(x, y, z);
    }

    @NonNull
    public void add(final Tensor tensor) {
        assert dimensions.equals(tensor.dimensions);
        for (int z = 0; z < dimensions.z; z++) {
            for (int y = 0; y < dimensions.y; y++) {
                for (int x = 0; x < dimensions.x; x++) {
                    data[x][y][z] += tensor.data[x][y][z];
                }
            }
        }
    }

    @NonNull
    public void subtract(final Tensor tensor) {
        assert dimensions.equals(tensor.dimensions);
        for (int z = 0; z < dimensions.z; z++) {
            for (int y = 0; y < dimensions.y; y++) {
                for (int x = 0; x < dimensions.x; x++) {
                    data[x][y][z] -= tensor.data[x][y][z];
                }
            }
        }
    }

    @NonNull
    public void applyFunction(TensorFunction function) {
        for (int z = 0; z < dimensions.z; z++) {
            for (int y = 0; y < dimensions.y; y++) {
                for (int x = 0; x < dimensions.x; x++) {
                    data[x][y][z] = function.apply(x, y, z);
                }
            }
        }
    }

    @NonNull
    public void copyFrom(Tensor from) {
        assert dimensions.equals(from.dimensions);
        for (int z = 0; z < dimensions.z; z++) {
            for (int y = 0; y < dimensions.y; y++) {
                for (int x = 0; x < dimensions.x; x++) {
                    data[x][y][z] = from.data[x][y][z];
                }
            }
        }
    }

    public double[] flatten() {
        final double[] flatten = new double[dimensions.x * dimensions.y * dimensions.z];
        for (int z = 0; z < dimensions.z; z++) {
            for (int y = 0; y < dimensions.y; y++) {
                for (int x = 0; x < dimensions.x; x++) {
                    flatten[z * (x + y) + y * x + x] = data[x][y][z];
                }
            }
        }
        return flatten;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    public double getValue(final int x, final int y, final int z) {
        return data[x][y][z];
    }

    public void setValue(final int x, final int y, final int z, final double value) {
        data[x][y][z] = value;
    }

    @Override
    public String toString() {
        return "Tensor{" +
                "data=" + Arrays.deepToString(data) +
                '}';
    }
}
