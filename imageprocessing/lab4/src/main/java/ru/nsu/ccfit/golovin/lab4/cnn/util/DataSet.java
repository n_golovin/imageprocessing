package ru.nsu.ccfit.golovin.lab4.cnn.util;


import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

public class DataSet {
    private final List<Instance> instances = new ArrayList<>();

    public int size() {
        return instances.size();
    }

    @NonNull
    public void addImage(final Instance inst) {
        instances.add(inst);
    }

    public List<Instance> getImages() {
        return new ArrayList<>(instances);
    }
}