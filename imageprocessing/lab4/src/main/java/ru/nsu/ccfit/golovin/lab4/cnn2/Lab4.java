package ru.nsu.ccfit.golovin.lab4.cnn2;

import ru.nsu.ccfit.golovin.lab4.cnn2.common.ActivationFunction;
import ru.nsu.ccfit.golovin.lab4.cnn2.common.DataSet;
import ru.nsu.ccfit.golovin.lab4.cnn2.common.Instance;
import ru.nsu.ccfit.golovin.lab4.cnn2.layer.ConvolutionLayer;
import ru.nsu.ccfit.golovin.lab4.cnn2.layer.ConvolutionalNeuralNetwork;
import ru.nsu.ccfit.golovin.lab4.cnn2.layer.PoolingLayer;
import ru.nsu.ccfit.golovin.lab4.cnn2.ui.MainFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lab4 {
    public static final int IMAGE_SIZE = 32;
    private static final double LEARNING_RATE = .1;
    private static final double DROPOUT_RATE = .0;
    private static final int MIN_EPOCHS = 50;
    private static final int MAX_EPOCHS = 100;

    public enum Classification {
        AIRPLANE("airplane"),
        FLOWER("flower"),
        WATCH("watch");

        private final String fileName;

        Classification(final String fileName) {
            this.fileName = fileName;
        }

        public static List<String> getCategoryNames() {
            List<String> categoryNames = new ArrayList<>();
            for (Classification classification : values()) {
                categoryNames.add(classification.fileName);
            }
            return categoryNames;
        }
    }

    public static void main(String[] args) {
        File trainSetDir = new File(Objects.requireNonNull(Lab4.class.getClassLoader().getResource("images/trainset/")).getPath());
        File tuneSetDir = new File(Objects.requireNonNull(Lab4.class.getClassLoader().getResource("images/tuneset/")).getPath());
        File testSetDir = new File(Objects.requireNonNull(Lab4.class.getClassLoader().getResource("images/testset/")).getPath());
        System.out.println(trainSetDir + " " + tuneSetDir + " " + testSetDir + " " + IMAGE_SIZE);

        long start = System.currentTimeMillis();
        DataSet trainSet = loadDataSet(trainSetDir);
        System.out.println("The trainset contains " + trainSet.getSize() + " examples.  Took " + (System.currentTimeMillis() - start) + " ms.");

        start = System.currentTimeMillis();
        DataSet tuneSet = loadDataSet(tuneSetDir);
        System.out.println("The tuneset contains " + tuneSet.getSize() + " examples.  Took " + (System.currentTimeMillis() - start) + " ms.");

        start = System.currentTimeMillis();
        DataSet testSet = loadDataSet(testSetDir);
        System.out.println("The testset contains " + testSet.getSize() + " examples.  Took " + (System.currentTimeMillis() - start) + " ms.");

        start = System.currentTimeMillis();
        ConvolutionalNeuralNetwork cnn = trainCnn(trainSet, tuneSet, testSet);
        System.out.println("\nTook " + (System.currentTimeMillis() - start) + " ms to train.");

        SwingUtilities.invokeLater(() -> new MainFrame(cnn).setVisible(true));
    }

    private static DataSet loadDataSet(File dir) {
        DataSet dataSet = new DataSet();
        for (File file : Objects.requireNonNull(dir.listFiles())) {
            if (!file.isFile() || !file.getName().endsWith(".jpg")) {
                continue;
            }
            BufferedImage img, scaledBI;
            try {
                img = ImageIO.read(file);
                String name = file.getName();
                int locationOfUnderscoreImage = name.indexOf("_image");

                scaledBI = new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = scaledBI.createGraphics();
                g.drawImage(img, 0, 0, IMAGE_SIZE, IMAGE_SIZE, null);
                g.dispose();

                Instance instance = new Instance(scaledBI, name.substring(0, locationOfUnderscoreImage));
                dataSet.addImage(instance);
            } catch (IOException e) {
                System.err.println("Error: cannot load in the image file");
                System.exit(1);
            }
        }
        return dataSet;
    }

    private static ConvolutionalNeuralNetwork trainCnn(DataSet trainSet, DataSet tuneSet, DataSet testSet) {
        ConvolutionalNeuralNetwork cnn = ConvolutionalNeuralNetwork.newBuilder()
                .setInputHeight(IMAGE_SIZE)
                .setInputWidth(IMAGE_SIZE)
                .appendConvolutionLayer(ConvolutionLayer.newBuilder()
                        .setConvolutionSize(4, 5, 5)
                        .setNumConvolutions(20)
                        .setDropoutRate(DROPOUT_RATE)
                        .build())
                .appendPoolingLayer(PoolingLayer.newBuilder()
                        .setWindowSize(2, 2)
                        .setNumWindows(20)
                        .setDropoutRate(DROPOUT_RATE)
                        .build())
                .appendConvolutionLayer(ConvolutionLayer.newBuilder()
                        .setConvolutionSize(1, 5, 5)
                        .setNumConvolutions(20)
                        .setDropoutRate(DROPOUT_RATE)
                        .build())
                .appendPoolingLayer(PoolingLayer.newBuilder()
                        .setWindowSize(2, 2)
                        .setNumWindows(20)
                        .setDropoutRate(DROPOUT_RATE)
                        .build())
                .setFullyConnectedDepth(1)
                .setFullyConnectedWidth(300)
                .setFullyConnectedActivationFunction(ActivationFunction.SIGMOID)
                .setClasses(Classification.getCategoryNames())
                .setMinEpochs(MIN_EPOCHS)
                .setMaxEpochs(MAX_EPOCHS)
                .setLearningRate(LEARNING_RATE)
                .setFullyConnectedDropoutRate(DROPOUT_RATE)
                .setUseRGB(true)
                .build();

        System.out.println("******\tCNN constructed." + " The structure is described below.\t******");
        System.out.println(cnn);

        System.out.println("******\tCNN training has begun." + " Updates will be provided after each epoch.\t******");
        System.out.println("Learning rate: " + LEARNING_RATE);
        cnn.train(trainSet, tuneSet);

        System.out.println("\n******\tCNN testing has begun.\t******");
        System.out.println(cnn.test(testSet, true) + "% accuracy");
        return cnn;
    }
}
