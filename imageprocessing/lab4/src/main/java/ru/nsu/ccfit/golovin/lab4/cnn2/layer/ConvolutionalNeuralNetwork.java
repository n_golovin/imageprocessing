package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import ru.nsu.ccfit.golovin.lab4.cnn2.common.ActivationFunction;
import ru.nsu.ccfit.golovin.lab4.cnn2.common.DataSet;
import ru.nsu.ccfit.golovin.lab4.cnn2.common.Instance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.*;

public class ConvolutionalNeuralNetwork {
    private final int inputHeight;
    private final int inputWidth;
    private final List<PlateLayer> plateLayers;
    private final List<FullyConnectedLayer> fullyConnectedLayers;
    private final List<String> classes;
    private final int minEpochs;
    private final int maxEpochs;
    private final double learningRate;

    private ConvolutionalNeuralNetwork(int inputHeight, int inputWidth, List<PlateLayer> plateLayers,
                                       List<FullyConnectedLayer> fullyConnectedLayers, List<String> classes,
                                       int minEpochs, int maxEpochs, double learningRate) {
        this.inputHeight = inputHeight;
        this.inputWidth = inputWidth;
        this.plateLayers = plateLayers;
        this.fullyConnectedLayers = fullyConnectedLayers;
        this.classes = classes;
        this.minEpochs = minEpochs;
        this.maxEpochs = maxEpochs;
        this.learningRate = learningRate;
    }

    public void train(DataSet trainSet, DataSet tuneSet) {
        double bestAccuracy = 0.0, prevAccuracy = 0.0, currAccuracy;
        for (int epoch = 1; epoch <= maxEpochs; epoch++) {
            trainSet = shuffle(trainSet);
            trainSingleEpoch(trainSet);

            currAccuracy = test(tuneSet, (epoch % 5 == 0));

            double trainAccuracy = test(trainSet, false);
            System.out.printf("Epoch %d completed with train accuracy of %.9f and tune accuracy of %.9f\n", epoch,
                    trainAccuracy, currAccuracy);

            if (currAccuracy > bestAccuracy) {
                saveLayerStates();
                bestAccuracy = currAccuracy;
            } else if (currAccuracy < prevAccuracy && trainAccuracy > .95 && epoch >= minEpochs) {
                restoreLayerStates();
                break;
            }

            prevAccuracy = currAccuracy;
        }
    }

    private void trainSingleEpoch(DataSet trainSet) {
        for (Instance img : trainSet.getImages()) {
            double[] output = computeOutput(img, true);
            double[] correctOutput = labelToOneOfN(img.getLabel());

            double[] fcError = tensorSubtract(output, correctOutput, false);
            for (int i = 0; i < fcError.length; i++) {
                fcError[i] *= ActivationFunction.SIGMOID.derivative(output[i]);
            }

            for (int i = fullyConnectedLayers.size() - 1; i >= 0; i--) {
                fcError = fullyConnectedLayers.get(i).propagateError(fcError, learningRate);
            }

            if (plateLayers.size() > 0) {
                PlateLayer lastPlate = plateLayers.get(plateLayers.size() - 1);
                List<Plate> plateErrors = unpackPlates(fcError, lastPlate.calculateOutputHeight(), lastPlate.calculateOutputWidth());
                for (int i = plateLayers.size() - 1; i >= 0; i--) {
                    plateErrors = plateLayers.get(i).propagateError(plateErrors, learningRate);
                }
            }
        }
    }

    private void saveLayerStates() {
        for (FullyConnectedLayer fcLayer : fullyConnectedLayers) {
            fcLayer.saveWeights();
        }

        for (PlateLayer plateLayer : plateLayers) {
            plateLayer.saveState();
        }
    }

    private void restoreLayerStates() {
        for (FullyConnectedLayer fcLayer : fullyConnectedLayers) {
            fcLayer.restoreWeights();
        }

        for (PlateLayer plateLayer : plateLayers) {
            plateLayer.restoreState();
        }
    }

    public double test(DataSet testSet, boolean outputConfusionMatrix) {
        int errCount = 0;
        int[][] confusionMatrix = new int[classes.size()][classes.size()];
        for (Instance img : testSet.getImages()) {
            String predicted = classify(img);
            if (!predicted.equals(img.getLabel())) {
                errCount++;
            }
            if (outputConfusionMatrix) {
                confusionMatrix[classes.indexOf(predicted)][classes.indexOf(img.getLabel())]++;
            }
        }

        double accuracy = ((double) (testSet.getSize() - errCount)) / testSet.getSize();

        if (outputConfusionMatrix) {
            System.out.println("\n\t------------\t\tConfusion Matrix\t\t------------\n");

            System.out.format("%15s", "");
            for (String label : classes) {
                System.out.format("%15s", label);
            }
            System.out.println();

            for (int i = 0; i < confusionMatrix.length; i++) {
                System.out.format("%15s", classes.get(i));
                for (int matrixElement : confusionMatrix[i]) {
                    System.out.format("%15d", matrixElement);
                }
                System.out.println();
            }

            System.out.println("\n\t--------------------------------------------------------------------\n");
        }

        return accuracy;
    }

    private String classify(Instance img) {
        double[] probs = computeOutput(img, false);
        double maxProb = -1;
        int bestIndex = -1;
        for (int i = 0; i < probs.length; i++) {
            if (probs[i] > maxProb) {
                maxProb = probs[i];
                bestIndex = i;
            }
        }
        return classes.get(bestIndex);
    }

    public double[] computeOutput(Instance img, boolean currentlyTraining) {
        List<Plate> plates = Arrays.asList(instanceToPlate(img));
        for (PlateLayer layer : plateLayers) {
            plates = layer.computeOutput(plates, currentlyTraining);
        }

        double[] vec = packPlates(plates);
        for (FullyConnectedLayer fcLayer : fullyConnectedLayers) {
            vec = fcLayer.computeOutput(vec, currentlyTraining);
        }
        return vec;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n//////\tNETWORK SPECIFICATIONS\t//////\n");
        builder.append(String.format("Input Height: %d\n", inputHeight));
        builder.append(String.format("Input Width: %d\n", inputWidth));
        builder.append(String.format("Number of plate layers: %d\n", plateLayers.size()));
        builder.append(
                String.format(
                        "Number of fully connected hidden layers: %d\n",
                        fullyConnectedLayers.size() - 1));
        builder.append(
                String.format("Predicts these classes: %s\n", classes));
        builder.append("\n//////\tNETWORK STRUCTURE\t//////\n");
        if (plateLayers.isEmpty()) {
            builder.append("\n------\tNo plate layers!\t------\n");
        } else {
            for (PlateLayer plateLayer : plateLayers) {
                builder.append(plateLayer.toString());
            }
        }
        for (FullyConnectedLayer fcLayer : fullyConnectedLayers) {
            builder.append(fcLayer.toString());
        }
        return builder.toString();
    }

    private double[] labelToOneOfN(String label) {
        double[] correctOutput = new double[classes.size()];
        correctOutput[classes.indexOf(label)] = 1;
        return correctOutput;
    }

    private Plate[] instanceToPlate(Instance instance) {
        return new Plate[] {
                new Plate(intImgToDoubleImg(instance.getRedChannel())),
                new Plate(intImgToDoubleImg(instance.getBlueChannel())),
                new Plate(intImgToDoubleImg(instance.getGreenChannel())),
                new Plate(intImgToDoubleImg(instance.getGrayImage()))
        };
    }

    private static double[][] intImgToDoubleImg(int[][] intImg) {
        double[][] dblImg = new double[intImg.length][intImg[0].length];
        for (int i = 0; i < dblImg.length; i++) {
            for (int j = 0; j < dblImg[i].length; j++) {
                dblImg[i][j] = ((double) 255 - intImg[i][j]) / 255;
            }
        }
        return dblImg;
    }

    private static double[] packPlates(List<Plate> plates) {
        checkNotEmpty(plates, "Plates to pack");
        int flattenedPlateSize = plates.get(0).getTotalNumValues();
        double[] result = new double[flattenedPlateSize * plates.size()];
        for (int i = 0; i < plates.size(); i++) {
            System.arraycopy(
                    plates.get(i).flatten(),
                    0,
                    result,
                    i * flattenedPlateSize,
                    flattenedPlateSize);
        }
        return result;
    }

    private static List<Plate> unpackPlates(double[] packedPlates, int plateHeight, int plateWidth) {
        List<Plate> plates = new ArrayList<>();
        int k = 0;
        while (k < packedPlates.length) {
            double[][] unpackedPlate = new double[plateHeight][plateWidth];
            for (int i = 0; i < plateHeight; i++) {
                for (int j = 0; j < plateWidth; j++) {
                    if (k < packedPlates.length) {
                        unpackedPlate[i][j] = packedPlates[k++];
                    } else {
                        throw new RuntimeException(
                                String.format(
                                        "Dimensions error. %d values in packedPlates, specified plate dimensions were %dx%d",
                                        packedPlates.length,
                                        plateHeight,
                                        plateWidth));
                    }
                }
            }
            plates.add(new Plate(unpackedPlate));
        }
        return plates;
    }

    public static Builder newBuilder() { return new Builder(); }

    public static class Builder {
        private final List<PlateLayer> plateLayers = new ArrayList<>();
        private List<String> classes = null;
        private int inputHeight = 0;
        private int inputWidth = 0;
        private int fullyConnectedWidth = 0;
        private int fullyConnectedDepth = 0;
        private ActivationFunction fcActivation = null;
        private int minEpochs = 0;
        private int maxEpochs = 0;
        private double learningRate = 0;
        private double fcDropoutRate = 0;
        private boolean useRGB = true;

        private Builder() {}

        public Builder setInputHeight(int height) {
            checkPositive(height, "Input height");
            this.inputHeight = height;
            return this;
        }

        public Builder setInputWidth(int width) {
            checkPositive(width, "Input width");
            this.inputWidth = width;
            return this;
        }

        public Builder appendConvolutionLayer(ConvolutionLayer layer) {
            return appendPlateLayer(layer);
        }

        public Builder appendPoolingLayer(PoolingLayer layer) {
            return appendPlateLayer(layer);
        }

        private Builder appendPlateLayer(PlateLayer layer) {
            Objects.requireNonNull(layer, "Plate layer");
            this.plateLayers.add(layer);
            return this;
        }

        public Builder setFullyConnectedWidth(int width) {
            checkPositive(width, "Fully connected width");
            this.fullyConnectedWidth = width;
            return this;
        }

        public Builder setFullyConnectedDepth(int depth) {
            checkPositive(depth, "Fully connected depth");
            this.fullyConnectedDepth = depth;
            return this;
        }

        public Builder setFullyConnectedActivationFunction(ActivationFunction fcActivation) {
            Objects.requireNonNull(fcActivation, "Fully connected activation function");
            this.fcActivation = fcActivation;
            return this;
        }

        public Builder setFullyConnectedDropoutRate(double fcDropoutRate) {
            if (fcDropoutRate < 0 || fcDropoutRate > 1) {
                throw new IllegalArgumentException(
                        String.format("Invalid dropout rate of %.2f\n", fcDropoutRate));
            }
            this.fcDropoutRate = fcDropoutRate;
            return this;
        }

        public Builder setClasses(List<String> classes) {
            Objects.requireNonNull(classes, "Classes");
            checkNotEmpty(classes, "Classes");
            this.classes = classes;
            return this;
        }

        public Builder setMinEpochs(int minEpochs) {
            checkPositive(minEpochs, "Min epochs");
            this.minEpochs = minEpochs;
            return this;
        }

        public Builder setMaxEpochs(int maxEpochs) {
            checkPositive(maxEpochs, "Max epochs");
            this.maxEpochs = maxEpochs;
            return this;
        }

        public Builder setLearningRate(double learningRate) {
            checkPositive(learningRate, "Learning rate");
            this.learningRate = learningRate;
            return this;
        }

        public Builder setUseRGB(boolean useRGB) {
            this.useRGB = useRGB;
            return this;
        }

        public ConvolutionalNeuralNetwork build() {
            Objects.requireNonNull(classes, "Classes");
            checkPositive(inputHeight, "Input height");
            checkPositive(inputWidth, "Input width");
            checkPositive(fullyConnectedWidth, "Fully connected width");
            checkPositive(fullyConnectedDepth, "Fully connected depth");
            Objects.requireNonNull(fcActivation, "Fully connected activation function");
            checkPositive(minEpochs, "Min epochs");
            checkPositive(maxEpochs, "Max epochs");
            checkPositive(learningRate, "Learning rate");

            int outputHeight = inputHeight;
            int outputWidth = inputWidth;
            int numOutputs = useRGB ? 4 : 1;
            for (PlateLayer plateLayer : plateLayers) {
                outputHeight = plateLayer.calculateOutputHeight(outputHeight);
                outputWidth = plateLayer.calculateOutputWidth(outputWidth);
                numOutputs = plateLayer.calculateNumOutputs(numOutputs);
            }

            List<FullyConnectedLayer> fullyConnectedLayers = new ArrayList<>(fullyConnectedDepth);

            int numInputs = outputWidth * outputHeight * numOutputs;
            numInputs = plateLayers.size() > 0
                    ? numInputs * (plateLayers.get(plateLayers.size() - 1)).getSize()
                    : numInputs;
            fullyConnectedLayers.add(FullyConnectedLayer.newBuilder()
                    .setActivationFunction(fcActivation)
                    .setDropoutRate(fcDropoutRate)
                    .setNumInputs(numInputs)
                    .setNumNodes(fullyConnectedWidth)
                    .build());

            for (int i = 0; i < fullyConnectedDepth - 1; i++) {
                fullyConnectedLayers.add(FullyConnectedLayer.newBuilder()
                        .setActivationFunction(fcActivation)
                        .setDropoutRate(fcDropoutRate)
                        .setNumInputs(fullyConnectedWidth)
                        .setNumNodes(fullyConnectedWidth)
                        .build());
            }

            fullyConnectedLayers.add(FullyConnectedLayer.newBuilder()
                    .setActivationFunction(ActivationFunction.SIGMOID)
                    .setNumInputs(fullyConnectedWidth)
                    .setNumNodes(classes.size())
                    .build());

            return new ConvolutionalNeuralNetwork(inputHeight, inputWidth, plateLayers, fullyConnectedLayers,
                    classes, minEpochs, maxEpochs, learningRate);
        }
    }
}
