package ru.nsu.ccfit.golovin.lab4.cnn2.common;


import java.awt.*;
import java.awt.image.BufferedImage;

public class Instance {
	private final BufferedImage image;
	private final String label;
	private final int width, height;
	private final int[][] redChannel, greenChannel, blueChannel;
	private int[][] grayImage;

	public Instance(BufferedImage image, String label) {
		this.image = image;
		this.label = label;
		this.width = image.getWidth();
        this.height = image.getHeight();

        this.redChannel = new int[height][width];
        this.greenChannel = new int[height][width];
        this.blueChannel = new int[height][width];

		for(int row = 0; row < height; ++row) {
			for(int col = 0; col < width; ++col) {
				Color c = new Color(image.getRGB(col, row));
                this.redChannel[  row][col] = c.getRed();
                this.greenChannel[row][col] = c.getGreen();
                this.blueChannel[ row][col] = c.getBlue();
			}
		}
	}

	public int[][] getRedChannel() {
		return redChannel;
	}

	public int[][] getGreenChannel() {
		return greenChannel;
	}

	public int[][] getBlueChannel() {
		return blueChannel;
	}

    public int[][] getGrayImage() {
		if(grayImage != null) {
			return grayImage;
		}

		grayImage = new int[height][width];

		for(int row = 0; row < height; ++row) {
			for(int col = 0; col < width; ++col) {
				int rgb = image.getRGB(col, row) & 0xFF;
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >>  8) & 0xFF;
				int b = (rgb        & 0xFF);
				grayImage[row][col] = (r + g + b) / 3;
			}
		}
		return grayImage;
	}

	public String getLabel() {
		return label;
	}
}
