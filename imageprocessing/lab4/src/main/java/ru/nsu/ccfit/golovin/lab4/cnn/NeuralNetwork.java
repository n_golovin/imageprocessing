package ru.nsu.ccfit.golovin.lab4.cnn;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab4.cnn.layer.Layer;
import ru.nsu.ccfit.golovin.lab4.cnn.util.DataSet;
import ru.nsu.ccfit.golovin.lab4.cnn.util.Instance;
import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor @Slf4j
public class NeuralNetwork {
    private final LinkedList<Layer> layers;
    private final List<String> classes;
    private final int minEpochs;
    private final int maxEpochs;

    public void train(final DataSet trainSet, final DataSet tuneSet) {
        Collections.shuffle(trainSet.getImages());
        double prevAccuracy = 0.0;
        double currAccuracy;
        for (int epoch = 1; epoch <= maxEpochs; epoch++) {
            trainSingleEpoch(trainSet);
            currAccuracy = test(tuneSet);
            log.info(String.format("Epoch %d completed with train accuracy of %.9f and tune accuracy of %.9f\n",
                    epoch, test(trainSet), currAccuracy));
            if (currAccuracy < prevAccuracy && epoch >= minEpochs) {
                break;
            }
            prevAccuracy = currAccuracy;
        }
    }

    private void trainSingleEpoch(DataSet trainSet) {
        for (Instance img : trainSet.getImages()) {
            layers.getFirst().getInput().copyFrom(img.getTensor());
            layers.forEach(Layer::forward);
            final Tensor output = layers.getLast().getOutput();
            final Tensor correctOutput = getCorrectOutput(img.getLabel());
            output.subtract(correctOutput);
            layers.getLast().getInputGradients().copyFrom(output);
            layers.forEach(Layer::backward);
        }
    }

    public double test(DataSet testSet) {
        int errCount = 0;
        for (Instance img : testSet.getImages()) {
            final String predicted = classify(img);
            if (!predicted.equals(img.getLabel())) {
                errCount++;
            }
//            log.info(String.format("Predicted: %s\t\tActual:%s\n", predicted, img.getLabel()));
        }
        final double accuracy = ((double) (testSet.size() - errCount)) / testSet.size();
        log.info(String.format("Final accuracy was %.9f\n", accuracy));
        return accuracy;
    }

    public String classify(final Instance img) {
        final double[] probs = computeOutput(img);
        double maxProb = -1;
        int bestIndex = -1;
        for (int i = 0; i < probs.length; i++) {
            if (probs[i] > maxProb) {
                maxProb = probs[i];
                bestIndex = i;
            }
        }
        return classes.get(bestIndex);
    }

    private double[] computeOutput(final Instance img) {
        layers.getFirst().getInput().copyFrom(img.getTensor());
        layers.forEach(Layer::forward);
        final Tensor output = layers.getLast().getOutput();
        return output.flatten();
    }

    private Tensor getCorrectOutput(final String label) {
        final Tensor correctOutput = new Tensor(classes.size(), 1, 1);
        correctOutput.setValue(classes.indexOf(label), 0, 0, 1.);
        return correctOutput;
    }
}
