package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import java.util.List;

public interface PlateLayer {
	int getSize();

	int calculateNumOutputs(int numInputs);

	int calculateOutputHeight(int inputHeight);

	int calculateOutputHeight();

	int calculateOutputWidth(int inputWidth);

	int calculateOutputWidth();

	List<Plate> computeOutput(List<Plate> input, boolean currentlyTraining);

	List<Plate> propagateError(List<Plate> errors, double learningRate);

	void saveState();

	void restoreState();
}
