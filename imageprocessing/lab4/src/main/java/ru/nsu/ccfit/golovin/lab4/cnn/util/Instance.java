package ru.nsu.ccfit.golovin.lab4.cnn.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Instance {
    private final BufferedImage image;
    private final String label;
    private final Tensor tensor;

    public Instance(BufferedImage image, String label) {
        this.image = image;
        this.label = label;
        this.tensor = new Tensor(image.getWidth(), image.getHeight(), 3);
        tensor.applyFunction((x, y, z) -> new Color(image.getRGB(x, y)).getRGBColorComponents(null)[z]);
    }

    public int getWidth() {
        return image.getWidth();
    }

    public int getHeight() {
        return image.getHeight();
    }

    public String getLabel() {
        return label;
    }

    public Tensor getTensor() {
        return tensor;
    }
}
