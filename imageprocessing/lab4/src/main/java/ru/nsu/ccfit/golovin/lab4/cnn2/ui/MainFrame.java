package ru.nsu.ccfit.golovin.lab4.cnn2.ui;

import ru.nsu.ccfit.golovin.lab4.cnn2.Lab4;
import ru.nsu.ccfit.golovin.lab4.cnn2.common.Instance;
import ru.nsu.ccfit.golovin.lab4.cnn2.layer.ConvolutionalNeuralNetwork;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.JOptionPane.showMessageDialog;
import static ru.nsu.ccfit.golovin.lab4.cnn2.Lab4.IMAGE_SIZE;

public class MainFrame extends JFrame {
    private JMenuBar menuBar;
    private JPanel imagePanel;
    private JPanel classificationPanel;

    private ConvolutionalNeuralNetwork cnn;
    private BufferedImage image;
    private JLabel imageLabel;
    private List<ClassificationRow> classificationRows;
    private ClassificationRow mostProbableClassificationRow;
    private JButton classifyButton;

    public MainFrame(ConvolutionalNeuralNetwork cnn) {
        this.cnn = cnn;

        setTitle("Lab4");
        setLayout(new GridBagLayout());
        setMinimumSize(new Dimension( 800, 600));
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.menuBar = createMenuBar();
        this.imagePanel = createImagePanel();
        this.classificationPanel = createClassificationPanel();

        setJMenuBar(menuBar);

        JScrollPane imagePanelScrollPane = new JScrollPane(imagePanel);
        imagePanelScrollPane.setBorder(BorderFactory.createTitledBorder("Image panel"));
        imagePanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.;
        constraints.weighty = 1.;
        add(imagePanelScrollPane, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1.e-2;
        constraints.weighty = 1.0;
        add(classificationPanel, constraints);
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        Font font = new Font("Verdana", Font.PLAIN, 12);
        JFileChooser fileChooser = new JFileChooser(getClass().getClassLoader().getResource("images").getPath());
        fileChooser.setFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));

        JMenuItem openMenuItem = new JMenuItem("Open");
        openMenuItem.setFont(font);
        openMenuItem.addActionListener(e -> {
            int actionStatus = fileChooser.showOpenDialog(this);
            if (actionStatus == JFileChooser.APPROVE_OPTION) {
                try {
                    image = ImageIO.read(fileChooser.getSelectedFile());
                    imageLabel.setIcon(new ImageIcon(image));
                    for (ClassificationRow classificationRow : classificationRows) {
                        classificationRow.reset();
                    }
                    mostProbableClassificationRow = null;
                } catch (Exception ex) {
                    System.err.println(String.format("Failed to open file '%s'", fileChooser.getSelectedFile().getAbsolutePath()));
                    showMessageDialog(getParent(), "Failed to open selected file", "Opening file error", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.setFont(font);
        exitMenuItem.addActionListener(e -> System.exit(0));

        JMenu fileMenu = new JMenu("File");
        fileMenu.setFont(font);
        fileMenu.add(openMenuItem);
        fileMenu.addSeparator();
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);
        return menuBar;
    }

    private JPanel createImagePanel() {
        JPanel imagePanel = new JPanel();
        imagePanel.setLayout(new GridBagLayout());

        imageLabel = new JLabel();
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.;
        constraints.weighty = 1.;

        imagePanel.add(imageLabel);
        return imagePanel;
    }

    private JPanel createClassificationPanel() {
        JPanel classificationPanel = new JPanel();
        classificationPanel.setLayout(new GridBagLayout());
        classificationPanel.setBorder(BorderFactory.createTitledBorder("Classification panel"));

        Insets insets = new Insets(5, 5, 5, 5);

        classificationRows = new ArrayList<>();
        for (int i = 0; i < Lab4.Classification.values().length; i++) {
            JCheckBox mostProbableCheckBox = new JCheckBox();
            mostProbableCheckBox.setEnabled(false);
            JLabel categoryNameLabel = new JLabel(Lab4.Classification.values()[i].toString());
            JLabel probabilityLabel = new JLabel("0.00%");

            GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridx = 0;
            constraints.gridy = i;
            constraints.weightx = .2;
            constraints.insets = insets;
            classificationPanel.add(mostProbableCheckBox, constraints);
            constraints.gridx = 1;
            constraints.weightx = .4;
            classificationPanel.add(categoryNameLabel, constraints);
            constraints.gridx = 2;
            classificationPanel.add(probabilityLabel, constraints);

            classificationRows.add(new ClassificationRow(mostProbableCheckBox, categoryNameLabel, probabilityLabel));
        }

        classifyButton = new JButton("Classify");
        classifyButton.addActionListener(e -> {
            if (image == null) {
                return;
            }
            BufferedImage scaledBI = new BufferedImage(IMAGE_SIZE, IMAGE_SIZE, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = scaledBI.createGraphics();
            g.drawImage(image, 0, 0, IMAGE_SIZE, IMAGE_SIZE, null);
            g.dispose();
            Instance instance = new Instance(scaledBI, null);
            double[] probabilities = cnn.computeOutput(instance, false);
            double maxProbability = -1;
            int maxProbabilityIndex = -1;
            for (int i = 0; i < probabilities.length; i++) {
                classificationRows.get(i).setProbability(probabilities[i]);
                if (probabilities[i] > maxProbability) {
                    maxProbability = probabilities[i];
                    maxProbabilityIndex = i;
                }
            }
            if (mostProbableClassificationRow != null) {
                mostProbableClassificationRow.markAsMostProbable(false);
            }
            classificationRows.get(maxProbabilityIndex).markAsMostProbable(true);
            mostProbableClassificationRow = classificationRows.get(maxProbabilityIndex);
        });
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridx = 0;
        constraints.gridy = Lab4.Classification.values().length;
        constraints.weightx = 1.;
        constraints.insets = insets;
        classificationPanel.add(classifyButton, constraints);

        return classificationPanel;
    }

    private static class ClassificationRow {
        private static final Font BOLD_FONT = new Font("VERDANA", Font.BOLD, 12);
        private static final Font PLAIN_FONT = new Font("VERDANA", Font.PLAIN, 12);

        private final JCheckBox mostProbableCheckBox;
        private final JLabel categoryNameLabel;
        private final JLabel probabilityLabel;

        public ClassificationRow(final JCheckBox mostProbableCheckBox, final JLabel categoryNameLabel, final JLabel probabilityLabel) {
            this.mostProbableCheckBox = mostProbableCheckBox;
            this.categoryNameLabel = categoryNameLabel;
            this.probabilityLabel = probabilityLabel;
        }

        void markAsMostProbable(boolean mostProbable) {
            mostProbableCheckBox.setSelected(mostProbable);
            if (mostProbable) {
                categoryNameLabel.setFont(BOLD_FONT);
                probabilityLabel.setFont(BOLD_FONT);
            } else {
                categoryNameLabel.setFont(PLAIN_FONT);
                probabilityLabel.setFont(PLAIN_FONT);
            }
        }

        void setProbability(double probability) {
            probabilityLabel.setText(String.format("%.2f%%", probability));
        }

        void reset() {
            markAsMostProbable(false);
            probabilityLabel.setText("0.00%");
        }
    }
}
