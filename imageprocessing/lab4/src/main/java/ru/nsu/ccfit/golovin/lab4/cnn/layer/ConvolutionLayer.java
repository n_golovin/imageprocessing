package ru.nsu.ccfit.golovin.lab4.cnn.layer;

import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;
import ru.nsu.ccfit.golovin.lab4.cnn.util.ActivationFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConvolutionLayer extends Layer {
    private final int stride;
    private final int filterSize;
    private final List<Tensor> filters;

    public ConvolutionLayer(final Tensor input, final Tensor outputGradients, final double learningRate, final int stride, final int filterSize, final int filtersNumber) {
        super(input, new Tensor(
                (input.getDimensions().x - filterSize) / stride + 1,
                (input.getDimensions().y - filterSize) / stride + 1,
                filtersNumber),
                new Tensor(
                        (input.getDimensions().x - filterSize) / stride + 1,
                        (input.getDimensions().y - filterSize) / stride + 1,
                        filtersNumber), outputGradients, learningRate);
        this.stride = stride;
        this.filterSize = filterSize;
        this.filters = new ArrayList<>(filtersNumber);
        for (int i = 0; i < filtersNumber; i++) {
            final Tensor filter = new Tensor(filterSize, filterSize, input.getDimensions().z);
            filter.applyFunction((x, y, z) -> .1);
            filters.add(filter);
        }
    }

    @Override
    public void forward() {
        output.applyFunction((x, y, z) -> {
            final Tensor filter = filters.get(z);
            double convolutedValue = 0.;
            for (int i = 0; i < filterSize; i++) {
                for (int j = 0; j < filterSize; j++) {
                    for (int k = 0; k < input.getDimensions().z; k++) {
                        convolutedValue += filter.getValue(i, j, k) * input.getValue(x * stride + i, y * stride + j, k);
                    }
                }
            }
            return ActivationFunction.RELU.apply(convolutedValue);
        });
//        System.out.println("Output: " + output);
    }

    @Override
    public void backward() {
        final List<Tensor> weightsFix = new ArrayList<>(Collections.nCopies(filters.size(), new Tensor(filterSize, filterSize, input.getDimensions().z)));
        outputGradients.applyFunction((x, y, z) -> 0.);
        inputGradients.applyFunction((x, y, z) -> {
            final Tensor filter = filters.get(z);
            final Tensor filterFix = weightsFix.get(z);
            for (int i = 0; i < filterSize; i++) {
                for (int j = 0; j < filterSize; j++) {
                    for (int k = 0; k < input.getDimensions().z; k++) {
                        final double gradientValue = outputGradients.getValue(x * stride + i, y * stride + j, k) +
                                (inputGradients.getValue(x, y, z) * filter.getValue(i, j, k) *
                                        ActivationFunction.RELU.derivative(input.getValue(x * stride + i, y * stride + j, k)));
                        final double weightValue = filterFix.getValue(i, j, k) + learningRate *
                                input.getValue(x * stride + i, y * stride + j, k) * inputGradients.getValue(x, y, z);
                        outputGradients.setValue(i, j, k, gradientValue);
                        filterFix.setValue(i, j, k, weightValue);
                    }
                }
            }
            return inputGradients.getValue(x, y, z);
        });
//        for (int i = 0; i < filters.size(); i++) {
//            filters.get(i).subtract(weightsFix.get(i));
//        }
//        System.out.println("Input gradients: " + inputGradients);
    }
}
