package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import ru.nsu.ccfit.golovin.lab4.cnn2.common.ActivationFunction;

import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ConvolutionLayer implements PlateLayer {
    private final List<Plate> convolutions;
    private final List<Plate> savedConvolutions;
    private final double dropoutRate;
    private final List<boolean[][]> activeNodes;

    private List<Plate> previousInput;
    private List<Plate> previousOutput;
    private int numChannels;
    private int outputHeight;
    private int outputWidth;
    private double[][] update;
    private List<Plate> deltaOutput;
    private List<Plate> output;
    private ActivationFunction activationFunction = ActivationFunction.RELU;

    private ConvolutionLayer(List<Plate> convolutions, int numChannels, double dropoutRate) {
        this.convolutions = convolutions;
        this.savedConvolutions = new ArrayList<>(deepCopyPlates(convolutions));
        this.numChannels = numChannels;

        this.dropoutRate = dropoutRate;
        this.activeNodes = new ArrayList<>(convolutions.size());
        for (Plate convolution : this.convolutions) {
            this.activeNodes.add(new boolean[convolution.getHeight()][convolution.getWidth()]);
        }
        resetDroppedOutNodes();
    }

    @Override
    public int getSize() {
        return convolutions.size();
    }

    @Override
    public int calculateNumOutputs(int numInputs) {
        return numInputs / numChannels;
    }

    @Override
    public int calculateOutputHeight(int inputHeight) {
        outputHeight = inputHeight - convolutions.get(0).getHeight() + 1;
        return outputHeight;
    }

    @Override
    public int calculateOutputHeight() {
        if (outputHeight > 0) {
            return outputHeight;
        } else {
            throw new RuntimeException(
                    "Cannot call calculateOutputHeight without arguments before calling it with arguments.");
        }
    }

    @Override
    public int calculateOutputWidth(int inputWidth) {
        outputWidth = inputWidth - convolutions.get(0).getWidth() + 1;
        return outputWidth;
    }

    @Override
    public int calculateOutputWidth() {
        if (outputWidth > 0) {
            return outputWidth;
        } else {
            throw new RuntimeException("Cannot call calculateOutputWidth without arguments before calling it with arguments.");
        }
    }

    @Override
    public List<Plate> computeOutput(List<Plate> input, boolean currentlyTraining) {
        Objects.requireNonNull(input, "Convolution layer input");
        checkNotEmpty(input, "Convolution layer input");

        previousInput = deepCopyPlates(input);

        if (currentlyTraining) {
            determineDroppedOutNodes();
        } else {
            resetDroppedOutNodes();
        }

        if (output == null) {
            int maskHeight = convolutions.get(0).getHeight();
            int maskWidth = convolutions.get(0).getWidth();
            int inputHeight = input.get(0).getHeight();
            int inputWidth = input.get(0).getWidth();
            output = new ArrayList<>();
            for (int i = 0; i < convolutions.size() / numChannels; i++) {
                output.add(new Plate(new double[inputHeight - maskHeight + 1][inputWidth - maskWidth + 1]));
            }
        } else {
            for (Plate plateOutput : output) {
                clear(plateOutput.getValues());
            }
        }

        for (int i = 0; i < output.size(); i++) {
            Plate outputPlate = output.get(i);
            for (int j = 0; j < input.size(); j++) {
                int convIndex = numChannels == 4 ? i * numChannels + j : i;
                Plate mask = convolutions.get(convIndex).rot180();
                if (currentlyTraining && dropoutRate != 0) {
                    mask = new Plate(scalarMultiply(1 - dropoutRate, mask.getValues(), false));
                }
                tensorAdd(outputPlate.getValues(), input.get(j).convolve(mask, activeNodes.get(convIndex)).getValues(), true);
            }
            outputPlate.applyActivation(activationFunction);
        }
        previousOutput = deepCopyPlates(output);

        return output;
    }


    @Override
    public List<Plate> propagateError(List<Plate> errors, double learningRate) {
        if (errors.size() != previousOutput.size() || previousInput.isEmpty()) {
            throw new IllegalArgumentException("Bad propagation state.");
        }

        if (update == null) {
            update = new double[convolutions.get(0).getHeight()][convolutions.get(0).getWidth()];
        }
        int errorHeight = errors.get(0).getHeight();
        int errorWidth = errors.get(0).getWidth();
        int convolutionHeight = convolutions.get(0).getHeight();
        int convolutionWidth = convolutions.get(0).getWidth();

        for (int i = 0; i < errors.size(); i++) {
            clear(update);
            for (int ii = 0; ii < numChannels; ii++) {
                Plate previous = previousInput.get((numChannels == 4) ? ii : i);
                for (int j = 0; j <= errorHeight - convolutionHeight; j++) {
                    for (int k = 0; k <= errorWidth - convolutionWidth; k++) {
                        for (int l = 0; l < convolutionHeight; l++) {
                            for (int m = 0; l < convolutionWidth; l++) {
                                if (!activeNodes.get(i)[l][m]) {
                                    continue;
                                }
                                update[l][m] += previous.valueAt(j + l, k + m)
                                        * errors.get(i).valueAt(j + l, k + m) * learningRate;
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < numChannels; j++) {
                tensorAdd(convolutions.get(i * numChannels + j).getValues(), update, true);
            }
        }

        if (deltaOutput == null) {
            deltaOutput = new ArrayList<>(errors.size());
            for (int i = 0; i < errors.size(); i++) {
                deltaOutput.add(new Plate(new double[previousInput.get(0).getHeight()][previousInput.get(0).getWidth()]));
            }
        } else {
            for (Plate deltaOutputPlate : deltaOutput) {
                clear(deltaOutputPlate.getValues());
            }
        }

        if (previousInput.size() != errors.size()) {
            return null;
        }

        for (int i = 0; i < errors.size(); i++) {
            Plate error = errors.get(i);
            for (int j = 0; j < deltaOutput.size(); j++) {
                double[][] delta = deltaOutput.get(j).getValues();
                Plate rotatedConvolution = convolutions.get(i).rot180();
                for (int row = 0; row <= delta.length - convolutionHeight; row++) {
                    for (int col = 0; col <= delta[row].length - convolutionWidth; col++) {
                        for (int kernelRow = 0; kernelRow < convolutionHeight; kernelRow++) {
                            for (int kernelCol = 0; kernelCol < convolutionWidth; kernelCol++) {
                                if (!activeNodes.get(i)[convolutions.get(i).getHeight() - kernelRow - 1][convolutions.get(i).getWidth() - kernelCol - 1]) {
                                    continue;
                                }
                                delta[row + kernelRow][col + kernelCol] += error.valueAt(row, col) * rotatedConvolution.valueAt(kernelRow, kernelCol);
                            }
                        }
                    }
                }
            }
        }

        for (int j = 0; j < deltaOutput.size(); j++) {
            double[][] delta = deltaOutput.get(j).getValues();
            Plate previous = previousInput.get(j);
            for (int row = 0; row < delta.length; row++) {
                for (int col = 0; col < delta[row].length; col++) {
                    delta[row][col] *= activationFunction.derivative(previous.valueAt(row, col));
                }
            }
        }
        return deltaOutput;
    }

    @Override
    public void saveState() {
        savedConvolutions.clear();
        savedConvolutions.addAll(deepCopyPlates(convolutions));
    }

    @Override
    public void restoreState() {
        convolutions.clear();
        convolutions.addAll(savedConvolutions);
    }

    private void determineDroppedOutNodes() {
        resetDroppedOutNodes();
        for (boolean[][] activeMatrix : activeNodes) {
            int dropoutCount = 0;
            int maxDropouts = activeMatrix.length * activeMatrix[0].length / 2;
            for (int i = 0; i < activeMatrix.length; i++) {
                for (int j = 0; j < activeMatrix[i].length; j++) {
                    if (dropoutRate > getRandomDouble() && dropoutCount < maxDropouts) {
                        activeMatrix[i][j] = false;
                        dropoutCount++;
                    }
                }
            }
        }
    }

    private void resetDroppedOutNodes() {
        for (boolean[][] activeMatrix : activeNodes) {
            for (int i = 0; i < activeMatrix.length; i++) {
                for (int j = 0; j < activeMatrix.length; j++) {
                    activeMatrix[i][j] = true;
                }
            }
        }
    }

    private static List<Plate> deepCopyPlates(List<Plate> plates) {
        List<Plate> deepCopy = new ArrayList<>(plates.size());
        for (Plate plate : plates) {
            double[][] plateValues = plate.getValues();
            double[][] copiedValues = new double[plateValues.length][plateValues[0].length];
            doubleArrayCopy2D(plateValues, copiedValues);
            deepCopy.add(new Plate(copiedValues));
        }
        return deepCopy;
    }

    @Override
    public String toString() {
        return "\n------\tConvolution Layer\t------\n\n" +
                String.format(
                        "Convolution Size: %dx%dx%d\n",
                        numChannels,
                        convolutions.get(0).getHeight(),
                        convolutions.get(0).getWidth()) +
                String.format(
                        "Output size     : %dx%dx%d\n",
                        numChannels,
                        outputHeight,
                        outputWidth) +
                String.format("Number of convolutions: %d\n", convolutions.size()) +
                "Activation Function: RELU\n" +
                String.format("Dropout rate: %.2f\n", dropoutRate) +
                "\n\t------------\t\n";
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private int numChannels = 0;
        private int convolutionHeight = 0;
        private int convolutionWidth = 0;
        private int numConvolutions = 0;
        private double dropoutRate = 0;

        private Builder() {
        }

        public Builder setConvolutionSize(int numChannels, int height, int width) {
            checkPositive(numChannels, "Convolution channels");
            checkPositive(height, "Convolution height");
            checkPositive(width, "Convolution width");
            this.numChannels = numChannels;
            this.convolutionHeight = height;
            this.convolutionWidth = width;
            return this;
        }

        public Builder setNumConvolutions(int numConvolutions) {
            checkPositive(numConvolutions, "Number of convolutions");
            this.numConvolutions = numConvolutions;
            return this;
        }

        public Builder setDropoutRate(double dropoutRate) {
            if (dropoutRate < 0 || dropoutRate > 1) {
                throw new IllegalArgumentException(
                        String.format("Dropout rate of %.2f is not valid.\n", dropoutRate));
            }
            this.dropoutRate = dropoutRate;
            return this;
        }

        public ConvolutionLayer build() {
            checkPositive(numChannels, "Convolution channels");
            checkPositive(convolutionHeight, "Convolution height");
            checkPositive(convolutionWidth, "Convolution width");
            checkPositive(numConvolutions, "Number of convolutions");

            List<Plate> convolutions = new ArrayList<>();
            for (int i = 0; i < numConvolutions; i++) {
                for (int j = 0; j < numChannels; j++) {
                    convolutions.add(new Plate(createRandomConvolution(convolutionHeight, convolutionWidth)));
                }
            }
            return new ConvolutionLayer(convolutions, numChannels, dropoutRate);
        }

        private static double[][] createRandomConvolution(int height, int width) {
            double[][] plateValues = new double[height][width];
            for (int i = 0; i < plateValues.length; i++) {
                for (int j = 0; j < plateValues[i].length; j++) {
                    plateValues[i][j] = getRandomGaussian();
                }
            }
            return plateValues;
        }
    }
}
