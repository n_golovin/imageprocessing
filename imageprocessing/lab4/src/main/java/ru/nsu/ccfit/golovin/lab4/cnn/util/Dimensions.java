package ru.nsu.ccfit.golovin.lab4.cnn.util;

import lombok.Value;

@Value
public class Dimensions {
    public final int x;
    public final int y;
    public final int z;
}
