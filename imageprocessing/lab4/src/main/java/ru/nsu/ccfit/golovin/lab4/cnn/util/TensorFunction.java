package ru.nsu.ccfit.golovin.lab4.cnn.util;

@FunctionalInterface
public interface TensorFunction {
    double apply(final int x, final int y, final int z);
}
