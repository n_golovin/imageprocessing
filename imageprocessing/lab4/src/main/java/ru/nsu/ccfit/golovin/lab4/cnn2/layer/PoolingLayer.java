package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import java.util.ArrayList;
import java.util.List;

import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.checkPositive;
import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.getRandomDouble;

public class PoolingLayer implements PlateLayer {
    private final int windowHeight;
    private final int windowWidth;
    private int outputHeight;
    private int outputWidth;
    private List<Plate> errors;
    private List<Plate> output;
    private double[][][] upscaledValues;
    private boolean[][][] windowMaximums;
    private final double dropoutRate;
    private final boolean[][] activeNodes;

    private PoolingLayer(int numWindows, int windowHeight, int windowWidth, double dropoutRate) {
        windowMaximums = new boolean[numWindows][][];
        this.windowHeight = windowHeight;
        this.windowWidth = windowWidth;

        this.dropoutRate = dropoutRate;
        this.activeNodes = new boolean[windowHeight][windowWidth];
        resetDroppedOutNodes();
    }

    @Override
    public int getSize() {
        return windowMaximums.length;
    }

    @Override
    public int calculateNumOutputs(int numInputs) {
        return numInputs;
    }

    @Override
    public int calculateOutputHeight(int inputHeight) {
        outputHeight = inputHeight / windowHeight;
        if (inputHeight % windowHeight > 0) {
            outputHeight++;
        }
        return outputHeight;
    }

    @Override
    public int calculateOutputHeight() {
        if (outputHeight > 0) {
            return outputHeight;
        } else {
            throw new RuntimeException("Cannot call calculateOutputHeight without arguments before calling it with arguments.");
        }
    }

    @Override
    public int calculateOutputWidth(int inputWidth) {
        outputWidth = inputWidth / windowWidth;
        if (inputWidth % windowWidth > 0) {
            outputWidth++;
        }
        return outputWidth;
    }

    @Override
    public int calculateOutputWidth() {
        if (outputWidth > 0) {
            return outputWidth;
        } else {
            throw new RuntimeException("Cannot call calculateOutputWidth without arguments before calling it with arguments.");
        }
    }


    @Override
    public List<Plate> computeOutput(List<Plate> input, boolean currentlyTraining) {
        if (currentlyTraining) {
            determineDroppedOutNodes();
        } else {
            resetDroppedOutNodes();
        }

        if (windowMaximums[0] == null) {
            windowMaximums = new boolean[input.size()][input.get(0).getHeight()][input.get(0).getWidth()];
        }
        int resultHeight = input.get(0).getHeight() / windowHeight;
        int resultWidth = input.get(0).getWidth() / windowWidth;
        resultHeight += input.get(0).getHeight() % windowHeight == 0 ? 0 : 1;
        resultWidth += input.get(0).getWidth() % windowWidth == 0 ? 0 : 1;
        if (output == null) {
            output = new ArrayList<>(input.size());
            for (int i = 0; i < input.size(); i++) {
                output.add(new Plate(new double[resultHeight][resultWidth]));
            }
        }

        for (int i = 0; i < input.size(); i++) {
            double[][] result = output.get(i).getValues();
            for (int j = 0; j < result.length; j++) {
                for (int k = 0; k < result[j].length; k++) {
                    int windowStartI = Math.min(j * windowHeight, input.get(i).getHeight() - 1);
                    int windowStartJ = Math.min(k * windowWidth, input.get(i).getWidth() - 1);
                    result[j][k] = maxValInWindow(input.get(i), windowMaximums[i], windowStartI, windowStartJ, windowHeight, windowWidth);
                }
            }
        }
        return output;
    }

    @Override
    public List<Plate> propagateError(List<Plate> gradients, double learningRate) {
        if (errors == null) {
            errors = new ArrayList<>(gradients.size());
            upscaledValues = new double[gradients.size()][windowMaximums[0].length][windowMaximums[0][0].length];
            for (int i = 0; i < gradients.size(); i++) {
                errors.add(new Plate(upscaledValues[i]));
            }
        }
        for (int i = 0; i < gradients.size(); i++) {
            Plate errorPlate = gradients.get(i);
            for (int j = 0; j < windowMaximums[i].length; j++) {
                for (int k = 0; k < windowMaximums[i][j].length; k++) {
                    upscaledValues[i][j][k] = windowMaximums[i][j][k] ? errorPlate.valueAt(j / windowHeight, k / windowWidth) : 0;
                }
            }
        }
        return errors;
    }

    @Override
    public void saveState() {}

    @Override
    public void restoreState() {}

    private double maxValInWindow(Plate plate, boolean[][] maximumOfPlate, int windowStartI, int windowStartJ, int windowHeight, int windowWidth) {
        double max = -Double.MAX_VALUE;
        int windowEndI = Math.min(windowStartI + windowHeight - 1, plate.getHeight() - 1);
        int windowEndJ = Math.min(windowStartJ + windowWidth - 1, plate.getWidth() - 1);
        int maxI = -1;
        int maxJ = -1;
        for (int i = windowStartI; i <= windowEndI; i++) {
            for (int j = windowStartJ; j <= windowEndJ; j++) {
                if (!activeNodes[i - windowStartI][j - windowStartJ]) {
                    continue;
                }

                double value = plate.getValues()[i][j];
                if (value > max) {
                    max = value;
                    maxI = i;
                    maxJ = j;
                }
            }
        }

        maximumOfPlate[maxI][maxJ] = true;
        return max;
    }

    private void determineDroppedOutNodes() {
        resetDroppedOutNodes();
        int dropoutCount = 0;
        int maxDropouts = windowHeight * windowWidth / 2;
        for (int i = 0; i < activeNodes.length; i++) {
            for (int j = 0; j < activeNodes[i].length; j++) {
                if (dropoutRate > getRandomDouble() && dropoutCount < maxDropouts) {
                    activeNodes[i][j] = false;
                    dropoutCount++;
                }
            }
        }
    }

    private void resetDroppedOutNodes() {
        for (int i = 0; i < activeNodes.length; i++) {
            for (int j = 0; j < activeNodes[i].length; j++) {
                activeNodes[i][j] = true;
            }
        }
    }

    @Override
    public String toString() {
        return  "\n------\tPooling Layer\t------\n\n" +
                String.format("Window size: %dx%d\n", windowHeight, windowWidth) +
                String.format("Output size: %dx%d\n", outputHeight, outputWidth) +
                String.format("Dropout rate: %.2f\n", dropoutRate) +
                "\n\t------------\t\n";
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private int windowHeight = 0;
        private int windowWidth = 0;
        private int numWindows = 0;
        private double dropoutRate = 0;

        private Builder() {}

        public Builder setWindowSize(int height, int width) {
            checkPositive(height, "Window height");
            checkPositive(width, "Window width");
            this.windowHeight = height;
            this.windowWidth = width;
            return this;
        }

        public Builder setNumWindows(int numWindows) {
            checkPositive(numWindows, "Number of windows");
            this.numWindows = numWindows;
            return this;
        }

        public Builder setDropoutRate(double dropoutRate) {
            if (dropoutRate < 0 || dropoutRate > 1) {
                throw new IllegalArgumentException(
                        String.format("Invalid dropout rate of %.2f.\n", dropoutRate));
            }
            this.dropoutRate = dropoutRate;
            return this;
        }

        public PoolingLayer build() {
            checkPositive(windowHeight, "Window height");
            checkPositive(windowWidth, "Window width");
            checkPositive(numWindows, "Number of windows");
            return new PoolingLayer(numWindows, windowHeight, windowWidth, dropoutRate);
        }
    }
}
