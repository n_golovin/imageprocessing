package ru.nsu.ccfit.golovin.lab4.cnn2.layer;

import ru.nsu.ccfit.golovin.lab4.cnn2.common.ActivationFunction;

import java.util.Objects;

import static ru.nsu.ccfit.golovin.lab4.cnn2.common.Util.*;

public class FullyConnectedLayer {
    private final double[][] weights;
    private final double[][] savedWeights;
    private final double[] lastInput;
    private final double[] lastOutput;
    private final ActivationFunction activation;
    private final double dropoutRate;
    private final boolean[] activeNodes;

    private FullyConnectedLayer(double[][] weights, ActivationFunction activation, double dropoutRate) {
        this.weights = weights;
        this.savedWeights = new double[weights.length][weights[0].length];
        this.lastInput = new double[weights[0].length];
        this.lastOutput = new double[weights.length];
        this.activation = activation;

        this.dropoutRate = dropoutRate;
        this.activeNodes = new boolean[weights.length];
        resetDroppedOutNodes();

        this.lastInput[this.lastInput.length - 1] = -1;

        doubleArrayCopy2D(weights, savedWeights);
    }

    public double[] computeOutput(double[] input, boolean currentlyTraining) {
        if (input.length != lastInput.length - 	1) {
            throw new IllegalArgumentException(String.format("Input length in fully connected layer was %d, should be %d.",
                    lastInput.length, input.length));
        }

        if (currentlyTraining) {
            determineDroppedOutNodes();
        } else {
            resetDroppedOutNodes();
        }

        System.arraycopy(input, 0, lastInput, 0, input.length);
        for (int i = 0; i < lastOutput.length; i++) {
            if (!activeNodes[i]) {
                continue;
            }

            double sum = 0;
            for (int j = 0; j < lastInput.length; j++) {
                double sumTerm = weights[i][j] * lastInput[j];
                if (!currentlyTraining) {
                    sumTerm *= (1 - dropoutRate);
                }
                sum += sumTerm;
            }
            lastOutput[i] = activation.activate(sum);
        }
        return lastOutput;
    }

    double[] propagateError(double[] proppedDelta, double learningRate) {
        if (proppedDelta.length != weights.length) {
            throw new IllegalArgumentException(
                    String.format(
                            "Got length %d delta, expected length %d!",
                            proppedDelta.length,
                            weights.length));
        }

        double[] delta = new double[weights[0].length - 1];
        for (int j = 0; j < delta.length; j++) {
            for (int i = 0; i < weights.length; i++) {
                if (!activeNodes[i]) {
                    continue;
                }
                delta[j] += proppedDelta[i] * weights[i][j] * activation.derivative(lastInput[j]);
            }
            delta[j] /= delta.length;
        }

        tensorSubtract(weights, scalarMultiply(learningRate, outerProduct(proppedDelta, lastInput), true), true);
        return delta;
    }

    private void determineDroppedOutNodes() {
        resetDroppedOutNodes();
        for (int i = 0; i < activeNodes.length; i++) {
            if (dropoutRate > getRandomDouble()) {
                activeNodes[i] = false;
            }
        }
    }

    private void resetDroppedOutNodes() {
        for (int i = 0; i < activeNodes.length; i++) {
            activeNodes[i] = true;
        }
    }

    void saveWeights() { doubleArrayCopy2D(weights, savedWeights); }

    void restoreWeights() { doubleArrayCopy2D(savedWeights, weights); }

    @Override
    public String toString() {
        return "\n------\tFully Connected Layer\t------\n\n" +
                String.format("Number of inputs: %d (plus a bias)\n", weights[0].length - 1) +
                String.format("Number of nodes: %d\n", weights.length) +
                String.format("Activation function: %s\n", activation.toString()) +
                String.format("Dropout rate: %.2f", dropoutRate) +
                "\n\t------------\t\n";
    }

    public static Builder newBuilder() { return new Builder(); }

    public static class Builder {
        private ActivationFunction func = null;
        private int numInputs = 0;
        private int numNodes = 0;
        private double dropoutRate = 0;

        private Builder() {}

        public Builder setActivationFunction(ActivationFunction func) {
            Objects.requireNonNull(func, "Fully connected activation function");
            this.func = func;
            return this;
        }

        public Builder setNumInputs(int numInputs) {
            checkPositive(numInputs, "Number of fully connected inputs");
            this.numInputs = numInputs;
            return this;
        }

        public Builder setNumNodes(int numNodes) {
            checkPositive(numNodes, "Number of fully connected nodes");
            this.numNodes = numNodes;
            return this;
        }

        public Builder setDropoutRate(double dropoutRate) {
            if (dropoutRate < 0 || dropoutRate > 1) {
                throw new IllegalArgumentException(
                        String.format("Invalid dropout rate of %.2f\n", dropoutRate));
            }
            this.dropoutRate = dropoutRate;
            return this;
        }

        public FullyConnectedLayer build() {
            Objects.requireNonNull(func, "Fully connected activation function");
            checkPositive(numInputs, "Number of fully connected inputs");
            checkPositive(numNodes, "Number of fully connected nodes");

            double[][] weights = new double[numNodes][numInputs + 1];
            for (int i = 0; i < weights.length; i++) {
                for (int j = 0; j < weights[i].length; j++) {
                    weights[i][j] = getRandomWeight(numInputs, numNodes);
                }
            }
            return new FullyConnectedLayer(weights, func, dropoutRate);
        }
    }
}
