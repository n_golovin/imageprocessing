package ru.nsu.ccfit.golovin.lab4.cnn.layer;

import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;

public abstract class Layer {
    protected final Tensor input;
    protected final Tensor output;
    protected final Tensor inputGradients;
    protected final Tensor outputGradients;
    protected final double learningRate;

    protected Layer(final Tensor input, final Tensor output, final Tensor inputGradients, final Tensor outputGradients, final double learningRate) {
        this.input = input;
        this.output = output;
        this.inputGradients = inputGradients;
        this.outputGradients = outputGradients;
        this.learningRate = learningRate;
    }

    public Tensor getInput() {
        return input;
    }

    public Tensor getOutput() {
        return output;
    }

    public Tensor getInputGradients() {
        return inputGradients;
    }

    public Tensor getOutputGradients() {
        return outputGradients;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public abstract void forward();

    public abstract void backward();
}
