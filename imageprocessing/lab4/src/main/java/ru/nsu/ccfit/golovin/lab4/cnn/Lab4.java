package ru.nsu.ccfit.golovin.lab4.cnn;

import ru.nsu.ccfit.golovin.lab4.cnn.layer.ConvolutionLayer;
import ru.nsu.ccfit.golovin.lab4.cnn.layer.FullyConnectedLayer;
import ru.nsu.ccfit.golovin.lab4.cnn.layer.Layer;
import ru.nsu.ccfit.golovin.lab4.cnn.layer.PoolingLayer;
import ru.nsu.ccfit.golovin.lab4.cnn.util.ActivationFunction;
import ru.nsu.ccfit.golovin.lab4.cnn.util.DataSet;
import ru.nsu.ccfit.golovin.lab4.cnn.util.Instance;
import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;

public class Lab4 {
    private static int imageSize = 32;

    private enum Category {
        AIRPLANE, BUTTERFLY, FLOWER, GRAND_PIANO, STARFISH, WATCH
    }

    private static final double LEARNING_RATE = 0.01;

    private static final int MIN_EPOCHS = 100;
    private static final int MAX_EPOCHS = 1000;

    private static final Map<String, Category> classes = Map.of(
            "airplanes", Category.AIRPLANE,
            "butterfly", Category.BUTTERFLY,
            "flower", Category.FLOWER,
            "grand_piano", Category.GRAND_PIANO,
            "starfish", Category.STARFISH,
            "watch", Category.WATCH);

    public static DataSet trainSet, tuneSet, testSet;

    public static void main(String[] args) {
        File trainsetDir = new File(Lab4.class.getClassLoader().getResource("images/trainset/").getPath());
        File tunesetDir = new File(Lab4.class.getClassLoader().getResource("images/tuneset/").getPath());
        File testsetDir = new File(Lab4.class.getClassLoader().getResource("images/testset/").getPath());
        System.out.println(trainsetDir + " " + tunesetDir + " " + testsetDir + " " + imageSize);

        trainSet = new DataSet();
        tuneSet = new DataSet();
        testSet = new DataSet();

        long start = System.currentTimeMillis();
        loadDataSet(trainSet, trainsetDir);
        System.out.println("The trainset contains " + comma(trainSet.size()) + " examples.  Took "
                + convertMillisecondsToTimeSpan(System.currentTimeMillis() - start) + ".");

        start = System.currentTimeMillis();
        loadDataSet(tuneSet, trainsetDir);
        System.out.println("The tuneset contains " + comma(tuneSet.size()) + " examples.  Took "
                + convertMillisecondsToTimeSpan(System.currentTimeMillis() - start) + ".");

        start = System.currentTimeMillis();
        loadDataSet(testSet, testsetDir);
        System.out.println("The testset contains " + comma(testSet.size()) + " examples.  Took "
                + convertMillisecondsToTimeSpan(System.currentTimeMillis() - start) + ".");

        start = System.currentTimeMillis();

        trainANN();
        System.out.println("\nTook " + convertMillisecondsToTimeSpan(System.currentTimeMillis() - start) + " to train.");
    }

    public static void loadDataSet(DataSet dataset, File dir) {
        for (File file : dir.listFiles()) {
            if (!file.isFile() || !file.getName().endsWith(".jpg")) {
                continue;
            }
            BufferedImage img, scaledBI = null;
            try {
                img = ImageIO.read(file);
                String name = file.getName();
                int locationOfUnderscoreImage = name.indexOf("_image");

                if (imageSize != 128) {
                    scaledBI = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = scaledBI.createGraphics();
                    g.drawImage(img, 0, 0, imageSize, imageSize, null);
                    g.dispose();
                }

                Instance instance = new Instance(scaledBI == null ? img : scaledBI,
                        name.substring(0, locationOfUnderscoreImage));

                dataset.addImage(instance);
            } catch (IOException e) {
                System.err.println("Error: cannot load in the image file");
                System.exit(1);
            }
        }
    }

    private static Category convertCategoryStringToEnum(String name) {
        final Category category = classes.get(name);
        if (category == null) {
            throw new Error("Unknown category: " + name);
        }
        return category;
    }

    private static int trainANN() {
        System.out.println("\nTime to start learning!");
        return trainDeep();
    }

    private static final long millisecInMinute = 60000;
    private static final long millisecInHour = 60 * millisecInMinute;
    private static final long millisecInDay = 24 * millisecInHour;

    public static String convertMillisecondsToTimeSpan(long millisec) {
        return convertMillisecondsToTimeSpan(millisec, 0);
    }

    public static String convertMillisecondsToTimeSpan(long millisec, int digits) {
        if (millisec == 0) {
            return "0 seconds";
        }
        if (millisec < 1000) {
            return comma(millisec) + " milliseconds";
        }
        if (millisec > millisecInDay) {
            return comma(millisec / millisecInDay) + " days and "
                    + convertMillisecondsToTimeSpan(millisec % millisecInDay, digits);
        }
        if (millisec > millisecInHour) {
            return comma(millisec / millisecInHour) + " hours and "
                    + convertMillisecondsToTimeSpan(millisec % millisecInHour, digits);
        }
        if (millisec > millisecInMinute) {
            return comma(millisec / millisecInMinute) + " minutes and "
                    + convertMillisecondsToTimeSpan(millisec % millisecInMinute, digits);
        }

        return truncate(millisec / 1000.0, digits) + " seconds";
    }

    public static String comma(long value) {
        return String.format("%,d", value);
    }

    public static String truncate(double d, int decimals) {
        double abs = Math.abs(d);
        if (abs > 1e13) {
            return String.format("%." + (decimals + 4) + "g", d);
        } else if (abs > 0 && abs < Math.pow(10, -decimals)) {
            return String.format("%." + decimals + "g", d);
        }
        return String.format("%,." + decimals + "f", d);
    }

    private static int trainDeep() {
        final Instance instance = trainSet.getImages().get(0);
        final ConvolutionLayer layer1 = new ConvolutionLayer(new Tensor(instance.getWidth(), instance.getHeight(), 3),
                new Tensor(instance.getWidth(), instance.getHeight(), 3), LEARNING_RATE, 1, 5, 32);
        final PoolingLayer layer2 = new PoolingLayer(layer1.getOutput(), layer1.getInputGradients(), LEARNING_RATE, 2, 2);
        final ConvolutionLayer layer3 = new ConvolutionLayer(layer2.getOutput(), layer2.getInputGradients(), LEARNING_RATE, 1, 5, 64);
        final PoolingLayer layer4 = new PoolingLayer(layer3.getOutput(), layer3.getInputGradients(), LEARNING_RATE, 2, 2);
        final FullyConnectedLayer layer5 = new FullyConnectedLayer(layer4.getOutput(), layer4.getInputGradients(), LEARNING_RATE, Category.values().length, ActivationFunction.SIGMOID);
        final LinkedList<Layer> layers = new LinkedList<>(Arrays.asList(layer1, layer2, layer3, layer4, layer5));
        final NeuralNetwork cnn = new NeuralNetwork(layers, new ArrayList<>(classes.keySet()), MIN_EPOCHS, MAX_EPOCHS);

        System.out.println("******\tDeep CNN constructed."
                + " The structure is described below.\t******");
        System.out.println(cnn);

        System.out.println("******\tDeep CNN training has begun."
                + " Updates will be provided after each epoch.\t******");
        cnn.train(trainSet, tuneSet);

        System.out.println("\n******\tDeep CNN testing has begun.\t******");
        System.out.println(cnn.test(testSet) + "% accuracy");
        return 0;
    }

}
