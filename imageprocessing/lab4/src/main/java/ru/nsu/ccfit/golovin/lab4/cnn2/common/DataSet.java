package ru.nsu.ccfit.golovin.lab4.cnn2.common;

import java.util.ArrayList;
import java.util.List;

public class DataSet {
	private final List<Instance> instances = new ArrayList<>();

	public int getSize() {
		return instances.size();
	}

	public void addImage(Instance inst) {
		instances.add(inst);
	}

	public List<Instance> getImages() {
		return instances;
	}
}
