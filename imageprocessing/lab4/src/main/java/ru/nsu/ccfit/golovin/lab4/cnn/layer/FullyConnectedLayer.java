package ru.nsu.ccfit.golovin.lab4.cnn.layer;

import ru.nsu.ccfit.golovin.lab4.cnn.util.Tensor;
import ru.nsu.ccfit.golovin.lab4.cnn.util.ActivationFunction;
import ru.nsu.ccfit.golovin.lab4.cnn.util.Dimensions;

public class FullyConnectedLayer extends Layer {
    private final ActivationFunction activationFunction;
    private final Tensor weights;

    public FullyConnectedLayer(final Tensor input, final Tensor outputGradients, final double learningRate, final int outputSize, final ActivationFunction activationFunction) {
        super(input, new Tensor(outputSize, 1, 1), new Tensor(outputSize, 1, 1), outputGradients, learningRate);
        this.activationFunction = activationFunction;
        final Dimensions inputDimensions = input.getDimensions();
        this.weights = new Tensor(outputSize, inputDimensions.x * inputDimensions.y * inputDimensions.z, 1);
        this.weights.applyFunction((x, y, z) -> .1);
    }

    @Override
    public void forward() {
        final Dimensions inputDimensions = input.getDimensions();
        output.applyFunction((x, y, z) -> {
            double weightedSum = 0.;
            for (int i = 0; i < inputDimensions.x; i++) {
                for (int j = 0; j < inputDimensions.y; j++) {
                    for (int k = 0; k < inputDimensions.z; k++) {
                        final int weightsIndex = k * (inputDimensions.x * inputDimensions.y) + j * (inputDimensions.x) + i;
                        weightedSum += input.getValue(i, j, k) * weights.getValue(x, weightsIndex, 0);
                    }
                }
            }
            return activationFunction.apply(weightedSum);
        });
        System.out.println("Output: " + output);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void backward() {
        final Dimensions inputDimensions = input.getDimensions();
        final Tensor weightsFix = new Tensor(inputGradients.getDimensions().x, inputDimensions.x * inputDimensions.y * inputDimensions.z, 1);
        outputGradients.applyFunction((x, y, z) -> 0.);
        inputGradients.applyFunction((x, y, z) -> {
            for (int i = 0; i < inputDimensions.x; i++) {
                for (int j = 0; j < inputDimensions.y; j++) {
                    for (int k = 0; k < inputDimensions.z; k++) {
                        final int weightsIndex = k * (inputDimensions.x * inputDimensions.y) + j * (inputDimensions.x) + i;
                        final double gradientValue = outputGradients.getValue(i, j, k) + (inputGradients.getValue(x, y, z)
                                * activationFunction.derivative(input.getValue(i, j, k)) * weights.getValue(x, weightsIndex, 0));
                        final double weightValue = learningRate * input.getValue(i, j, k) * inputGradients.getValue(x, y, z);
                        outputGradients.setValue(i, j, k, gradientValue);
                        weightsFix.setValue(x, weightsIndex, 0, weightValue);
                    }
                }
            }
            return inputGradients.getValue(x, y, z);
        });
        weights.subtract(weightsFix);
//        System.out.println("Input gradients: " + inputGradients);
    }
}
