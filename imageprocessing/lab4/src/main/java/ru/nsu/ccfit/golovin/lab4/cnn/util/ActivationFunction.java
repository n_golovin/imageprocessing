package ru.nsu.ccfit.golovin.lab4.cnn.util;

import java.util.function.DoubleUnaryOperator;

import static java.lang.Math.exp;
import static java.lang.Math.max;

public enum ActivationFunction {
    RELU(x -> max(x, 0.), x -> x > 0. ? 1. : 0.),
    SIGMOID(x -> 1. / (1. + exp(-x)), x -> x * (1. - x));

    private final DoubleUnaryOperator activation;
    private final DoubleUnaryOperator derivative;

    ActivationFunction(final DoubleUnaryOperator activation, final DoubleUnaryOperator derivative) {
        this.activation = activation;
        this.derivative = derivative;
    }

    public double apply(final double x) {
        return activation.applyAsDouble(x);
    }

    public double derivative(final double x) {
        return derivative.applyAsDouble(x);
    }
}
