package ru.nsu.ccfit.golovin.lab4.cnn2.common;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static java.lang.Math.max;
import static java.lang.Math.sqrt;

public final class Util {
	private static final Random RANDOM = new Random(0);
	private static final Random WEIGHTS_RANDOM = new Random(638 * 838);

	private Util() {}

	public static double[][] outerProduct(double[] v1, double[] v2) {
		checkVectorNotNullOrEmpty(v1);
		checkVectorNotNullOrEmpty(v2);
		double[][] result = new double[v1.length][v2.length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = v1[i] * v2[j];
			}
		}
		return result;
	}

	public static double getRandomDouble() {
		return RANDOM.nextDouble();
	}

	public static double getRandomGaussian() {
		return RANDOM.nextGaussian();
	}

	public static double[][] scalarMultiply(double scalar, double[][] matrix, boolean inline) {
		return scalarMultiply(scalar, new double[][][]{ matrix }, inline)[0];
	}

	private static double[][][] scalarMultiply(double scalar, double[][][] tensor, boolean inline) {
		checkTensorNotNullOrEmpty(tensor);
		double[][][] result = inline
				? tensor
				: new double[tensor.length][tensor[0].length][tensor[0][0].length];
		for (int i = 0; i < tensor.length; i++) {
			for (int j = 0; j < tensor[i].length; j++) {
				for (int k = 0; k < tensor[i][j].length; k++) {
					result[i][j][k] = tensor[i][j][k] * scalar;
				}
			}
		}
		return result;
	}

	public static double[] tensorSubtract(double[] v1, double[] v2, boolean inline) {
		return tensorSubtract(new double[][][] {{ v1 }}, new double[][][]{{ v2 }}, inline)[0][0];
	}

	public static void tensorSubtract(double[][] m1, double[][] m2, boolean inline) {
		tensorSubtract(new double[][][]{m1}, new double[][][]{m2}, inline);
	}

	private static double[][][] tensorSubtract(double[][][] t1, double[][][] t2, boolean inline) {
		return tensorAdd(t1, scalarMultiply(-1, t2, inline), inline);
	}

	public static void tensorAdd(double[][] m1, double[][] m2, boolean inline) {
		tensorAdd(new double[][][]{m1}, new double[][][]{m2}, inline);
	}

	private static double[][][] tensorAdd(double[][][] t1, double[][][] t2, boolean inline) {
		checkTensorNotNullOrEmpty(t1);
		checkTensorNotNullOrEmpty(t2);
		checkTensorDimensionsMatch(t1, t2);
		double[][][] result = inline ? t1 : new double[t1.length][t1[0].length][t1[0][0].length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				for (int k = 0; k < result[i][j].length; k++) {
					result[i][j][k] = t1[i][j][k] + t2[i][j][k];
				}
			}
		}
		return result;
	}

	private static void checkVectorNotNullOrEmpty(double[] vector) {
        Objects.requireNonNull(vector, "Vector arg");
		checkPositive(vector.length, "Vector length");
	}

	private static void checkTensorNotNullOrEmpty(double[][][] tensor) {
		Objects.requireNonNull(tensor, "Tensor arg");
		checkPositive(tensor.length, "Tensor dimension 1");
		checkPositive(tensor[0].length, "Tensor dimension 2");
		checkPositive(tensor[0][0].length, "Tensor dimension 3");
	}
	
	private static void checkTensorDimensionsMatch(double[][][] t1, double[][][] t2) {
		if (t1.length != t2.length || t1[0].length != t2[0].length || t1[0][0].length != t2[0][0].length) {
            throw new IllegalArgumentException(String.format("Tensor dimensions do not match...\tT1:%dx%dx%d\tT2:%dx%dx%d\n",
                    t1.length, t1[0].length, t1[0][0].length, t2.length, t2[0].length, t2[0][0].length));
        }
	}
	
	public static void checkValueInRange(int val, int min, int max, String name) {
		if (val < min || val >= max) {
			throw new IllegalArgumentException(String.format("%s was %d, but should be in range [%d, %d)", name, val, min, max));
		}
	}
	
	public static void checkPositive(double val, String name) {
		if (val <= 0.) {
			throw new IllegalArgumentException(String.format("%s must be set to positive value!", name));
		}
	}
	
	public static void checkNotEmpty(Collection<?> coll, String name) {
		if (coll == null || coll.isEmpty()) {
			throw new IllegalArgumentException(String.format("%s must be nonempty!", name));
		}
	}

	public static void doubleArrayCopy2D(double[][] src, double[][] dst) {
        Objects.requireNonNull(src, "Array copy source.");
        Objects.requireNonNull(dst, "Array copy source.");
		checkPositive(src.length, "Source dimension 1");
		checkPositive(src[0].length, "Source dimension 2");
		checkPositive(dst.length, "Destination dimension 1");
		checkPositive(dst[0].length, "Destination dimension 2");
		for (int i = 0; i < src.length; i++) {
			System.arraycopy(src[i], 0, dst[i], 0, src[i].length);
		}
	}
	
	public static void clear(double[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = 0;
			}
		}
	}

	public static DataSet shuffle(DataSet original) {
		DataSet shuffled = new DataSet();
		List<Instance> instances = original.getImages();
		boolean[] positionsTaken = new boolean[instances.size()];
		int position;
		for (int i = 0; i < instances.size(); i++) {
			do {
				position = (int) (getRandomDouble() * positionsTaken.length);
			} while (positionsTaken[position]);

			shuffled.addImage(instances.get(position));
			positionsTaken[position] = true;
		}
		return shuffled;
	}

    public static double getRandomWeight(int fanin, int fanout) {
        double range = max(Double.MIN_VALUE, 4.0 / sqrt(6.0 * (fanin + fanout)));
        return (2.0 * WEIGHTS_RANDOM.nextDouble() - 1.0) * range;
    }
}
