package ru.nsu.ccfit.golovin.lab1.domain.color;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nsu.ccfit.golovin.lab1.util.ColorUtils;

import java.awt.*;

@Data @NoArgsConstructor
public class Pixel {
    private int x;
    private int y;
    private int[] rgb;
    private float[] hsv;
    private float[] lab;

    public Pixel(final int x, final int y, final Color color) {
        this.x = x;
        this.y = y;
        this.rgb = ColorUtils.colorToRgb(color);
        this.hsv = ColorUtils.rgbToHsv(rgb);
        this.lab = ColorUtils.rgbToLab(rgb);
    }
}
