package ru.nsu.ccfit.golovin.lab1.ui.observer;

import java.io.File;

public interface MenuBarObserver {
    void handleFileOpening(File file);
}
