package ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.ejml.simple.SimpleEVD;
import org.ejml.simple.SimpleMatrix;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.ImageSegmenter;
import ru.nsu.ccfit.golovin.lab1.util.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.lang.StrictMath.exp;
import static java.lang.StrictMath.pow;
import static ru.nsu.ccfit.golovin.lab1.util.ColorUtils.*;

@Slf4j
public class NormalizedCuts implements ImageSegmenter {
    private static final int SEGMENTATION_RADIUS = 8;

    @Override
    @NonNull
    public BufferedImage applySegmentation(final BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();

        final SimpleMatrix W = new SimpleMatrix(w * h, w * h);
        final SimpleMatrix D = new SimpleMatrix(w * h, w * h);
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                double weightSum = 0.;
                final float[] lab = colorToLab(new Color(image.getRGB(x, y)));
                for (int i = 0; i < 2 * SEGMENTATION_RADIUS; i++) {
                    for (int j = 0; j < 2 * SEGMENTATION_RADIUS; j++) {
                        final int currentX = x + (i - SEGMENTATION_RADIUS);
                        final int currentY = y + (j - SEGMENTATION_RADIUS);
                        if (currentX < 0 || currentX >= image.getWidth() || currentY < 0 || currentY >= image.getHeight()) {
                            continue;
                        }
                        final Color currentColor = new Color(image.getRGB(currentX, currentY));
                        final float[] currentLab = rgbToLab(colorToRgb(currentColor));
                        final double distance = computeDistanceBetweenColors(lab, currentLab);
                        final double weight = computeColorWeight(distance);
                        weightSum += weight;
                        W.set(w * y + x, w * currentY + currentX, weight);
                    }
                }
                D.set(w * y + x, w * y + x, weightSum);
            }
        }
        final SimpleEVD eig = D.invert().mult(D.minus(W)).eig();
        final int normalizedCutPosition = findSecondMinimumEigenvaluePosition(eig);
        final SimpleMatrix normalizedCut = eig.getEigenVector(normalizedCutPosition);
        final BufferedImage segmentedImage = new BufferedImage(w, h, image.getType());
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                if (normalizedCut.get(y * w + x, 0) <= 0) {
                    segmentedImage.setRGB(x, y, RED.getRGB());
                }
                if (normalizedCut.get(y * w + x, 0) >= 0) {
                    segmentedImage.setRGB(x, y, BLUE.getRGB());
                }
            }
        }
        ImageUtils.save(segmentedImage, "jpg", "normalized_cut");
        return segmentedImage;
    }

    @NonNull
    private static int findSecondMinimumEigenvaluePosition(final SimpleEVD eig) {
        double firstMin = Double.MAX_VALUE;
        double secondMin = Double.MAX_VALUE;
        int firstMinPosition = eig.getIndexMin();
        int secondMinPosition = eig.getIndexMin();
        for (int i = 0; i < eig.getNumberOfEigenvalues(); i++) {
            final double currentEigenvalue = eig.getEigenvalue(i).getMagnitude();
            if (currentEigenvalue < firstMin) {
                secondMin = firstMin;
                secondMinPosition = firstMinPosition;
                firstMin = currentEigenvalue;
                firstMinPosition = i;
            } else if (currentEigenvalue < secondMin) {
                secondMin = currentEigenvalue;
                secondMinPosition = i;
            }
        }
        return secondMinPosition;
    }

    private static double computeColorWeight(final double distance){
        return exp(-0.5 * pow(distance / SEGMENTATION_RADIUS, 2.));
    }
}
