package ru.nsu.ccfit.golovin.lab1.ui.observable;

import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFeaturesPanelObserver;

import java.awt.image.BufferedImage;

public interface ImageFeaturesPanelObservable extends UIObservable<ImageFeaturesPanelObserver> {
    void notifyAboutHsvChanging(BufferedImage image);
}
