package ru.nsu.ccfit.golovin.lab1.ui.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import ru.nsu.ccfit.golovin.lab1.domain.color.Pixel;
import ru.nsu.ccfit.golovin.lab1.ui.observable.ImageFeaturesPanelObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observer.*;
import ru.nsu.ccfit.golovin.lab1.util.ColorUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.join;

@Slf4j
public class ImageFeaturesPanel extends JPanel implements ImageFeaturesPanelObservable, ImagePanelObserver, ImageFiltersPanelObserver,
        ImageSegmentationPanelObserver, MenuBarObserver {
    private static final List<ImageFeaturesPanelObserver> observers = new ArrayList<>();
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);
    private static final List<String> LAB_COMPONENTS = List.of("L", "A", "B");

    private final JTextField[] positionTextFields = new JTextField[2];
    private JTextField rgbOutputTextField;
    private final JTextField[] rgbTextFields = new JTextField[3];
    private final JTextField[] hsvTextFields = new JTextField[3];
    private final JTextField[] labTextFields = new JTextField[3];
    private JTextArea selectedAreaPixelsTextArea;
    private JCheckBox normalizedHsvTransformationCheckBox;
    private JSlider hueSlider;
    private JSlider saturationSlider;
    private JSlider valueSlider;
    private ButtonGroup histogramTypeButtonGroup;
    private ChartPanel histogramChartPanel;

    private BufferedImage transformedImage;
    private BufferedImage originalImage;

    public ImageFeaturesPanel() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder("Image Features Panel"));

        addMouseClickingPosition();
        addRgbOutput();
        addColorFields(rgbTextFields, "RGB", 1);
        addColorFields(hsvTextFields, "HSV", 2);
        addColorFields(labTextFields, "LAB", 3);
        addSelectedAreaPixels();
        addNormalizedHsvTransformation();
        addHueSlider();
        addSaturationSlider();
        addValueSlider();
        addHistogramTypeButtonGroup();
        addHistogram();
    }

    private void addMouseClickingPosition() {
        final JLabel positionLabel = new JLabel("pos");

        final GridBagConstraints positionLabelConstraints = new GridBagConstraints();
        positionLabelConstraints.gridx = 0;
        positionLabelConstraints.gridy = 0;
        positionLabelConstraints.weightx = 1.;
        positionLabelConstraints.weighty = 1.;
        positionLabelConstraints.insets = DEFAULT_INSETS;

        add(positionLabel, positionLabelConstraints);

        for (int i = 0; i < positionTextFields.length; i++) {
            positionTextFields[i] = new JTextField(5);
            positionTextFields[i].setEditable(false);
            positionTextFields[i].setBorder(BorderFactory.createEmptyBorder());

            final GridBagConstraints positionTextFieldConstraints = new GridBagConstraints();
            positionTextFieldConstraints.gridx = 1 + i;
            positionTextFieldConstraints.gridy = 0;
            positionTextFieldConstraints.weightx = 1.;
            positionTextFieldConstraints.weighty = 1.;
            positionTextFieldConstraints.insets = DEFAULT_INSETS;

            add(positionTextFields[i], positionTextFieldConstraints);
        }
    }

    private void addRgbOutput() {
        rgbOutputTextField = new JTextField(5);
        rgbOutputTextField.setEditable(false);
        rgbOutputTextField.setBorder(BorderFactory.createEmptyBorder());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 3;
        constraints.gridy = 0;
        constraints.weightx = 1.;
        constraints.weighty = 1.;
        constraints.insets = DEFAULT_INSETS;

        add(rgbOutputTextField, constraints);
    }

    private void addColorFields(final JTextField[] fields, final String labelText, final int gridY) {
        final JLabel labLabel = new JLabel(labelText);

        final GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.gridx = 0;
        labelConstraints.gridy = gridY;
        labelConstraints.weightx = 1.;
        labelConstraints.weighty = 1.;
        labelConstraints.gridwidth = 1;
        labelConstraints.insets = DEFAULT_INSETS;

        add(labLabel, labelConstraints);

        for (int i = 0; i < fields.length; i++) {
            fields[i] = new JTextField(5);
            fields[i].setBorder(BorderFactory.createEmptyBorder());
            fields[i].setEditable(false);

            final GridBagConstraints textFieldConstraints = new GridBagConstraints();
            textFieldConstraints.gridx = i + 1;
            textFieldConstraints.gridy = gridY;
            textFieldConstraints.weightx = 1.;
            textFieldConstraints.weighty = 1.;
            textFieldConstraints.insets = DEFAULT_INSETS;

            add(fields[i], textFieldConstraints);
        }
    }

    private void addSelectedAreaPixels() {
        selectedAreaPixelsTextArea = new JTextArea(10, 30);
        selectedAreaPixelsTextArea.setEditable(true);
        final JScrollPane selectedAreaPixelsScrollPane = new JScrollPane(selectedAreaPixelsTextArea);
        selectedAreaPixelsScrollPane.setBorder(BorderFactory.createTitledBorder("Selected area pixels"));
        selectedAreaPixelsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.weightx = 1.;
        constraints.weighty = 1.;
        constraints.gridwidth = 4;
        constraints.insets = DEFAULT_INSETS;

        add(selectedAreaPixelsScrollPane, constraints);
    }

    private void addNormalizedHsvTransformation() {
        normalizedHsvTransformationCheckBox = new JCheckBox("Normalized HSV transformation", true);

        final GridBagConstraints textFieldConstraints = new GridBagConstraints();
        textFieldConstraints.gridx = 0;
        textFieldConstraints.gridy = 5;
        textFieldConstraints.weightx = 1.;
        textFieldConstraints.weighty = 1.;
        textFieldConstraints.gridwidth = 4;
        textFieldConstraints.insets = DEFAULT_INSETS;

        add(normalizedHsvTransformationCheckBox, textFieldConstraints);
    }

    private void addHueSlider() {
        hueSlider = new JSlider(JSlider.HORIZONTAL, -180, 180, 0);
        addSlider(hueSlider, 15, 60, "Hue", 6);
    }

    private void addSaturationSlider() {
        saturationSlider = new JSlider(JSlider.HORIZONTAL, -100, 100, 0);
        addSlider(saturationSlider, 10, 50, "Saturation", 7);
    }

    private void addValueSlider() {
        valueSlider = new JSlider(JSlider.HORIZONTAL, -100, 100, 0);
        addSlider(valueSlider, 10, 50, "Value", 8);
    }

    private void addSlider(final JSlider slider, final int minorTick, final int majorTick, final String title, final int gridY) {
        slider.setPaintLabels(true);
        slider.setMinorTickSpacing(minorTick);
        slider.setMajorTickSpacing(majorTick);
        slider.setPaintTicks(true);
        slider.setBorder(BorderFactory.createTitledBorder(title));
        slider.addChangeListener(e -> {
            if (slider.getValueIsAdjusting() || originalImage == null) {
                return;
            }
            if (normalizedHsvTransformationCheckBox.isSelected()) {
                transformedImage = ColorUtils.normalizedTransformImageHSV(originalImage, hueSlider.getValue(),
                        (saturationSlider.getValue() + saturationSlider.getMaximum()) / ((float) saturationSlider.getMaximum()),
                        (valueSlider.getValue() + valueSlider.getMaximum()) / ((float) valueSlider.getMaximum()));
            } else {
                transformedImage = ColorUtils.unnormalizedTransformImageHSV(originalImage, hueSlider.getValue(), saturationSlider.getValue(), valueSlider.getValue());
            }
            notifyAboutHsvChanging(transformedImage);
            updateHistogram();
        });

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = gridY;
        constraints.weightx = 1.;
        constraints.weighty = 1.;
        constraints.gridwidth = 4;
        constraints.insets = DEFAULT_INSETS;

        add(slider, constraints);
    }

    private void addHistogramTypeButtonGroup() {
        final JLabel histogramTypeLabel = new JLabel("Histogram type");

        final GridBagConstraints labelConstraints = new GridBagConstraints();
        labelConstraints.gridx = 0;
        labelConstraints.gridy = 9;
        labelConstraints.weightx = 1.;
        labelConstraints.weighty = 1.;
        labelConstraints.insets = DEFAULT_INSETS;

        add(histogramTypeLabel, labelConstraints);

        histogramTypeButtonGroup = new ButtonGroup();
        for (int i = 0; i < LAB_COMPONENTS.size(); i++) {
            addHistogramTypeRadioButton(LAB_COMPONENTS.get(i), i == 0, i + 1);
        }
    }

    private void addHistogramTypeRadioButton(final String radioButtonText, final boolean selected, final int gridX) {
        final JRadioButton histogramTypeRadioButton = new JRadioButton(radioButtonText, selected);
        histogramTypeRadioButton.setActionCommand(radioButtonText);
        histogramTypeRadioButton.addActionListener(e -> updateHistogram());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = gridX;
        constraints.gridy = 9;
        constraints.weightx = 1.;
        constraints.weighty = 1.;
        constraints.insets = DEFAULT_INSETS;

        histogramTypeButtonGroup.add(histogramTypeRadioButton);
        add(histogramTypeRadioButton, constraints);
    }

    private void addHistogram() {
        histogramChartPanel = new ChartPanel(null);
        histogramChartPanel.setPreferredSize(new Dimension(350, 350));

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 10;
        constraints.weighty = 1.;
        constraints.weightx = 1.;
        constraints.gridwidth = 4;

        add(histogramChartPanel, constraints);
    }

    private void updateHistogram() {
        if (originalImage == null) {
            return;
        }
        final String labComponent = histogramTypeButtonGroup.getSelection().getActionCommand();
        final double[] histogramValues = new double[transformedImage.getWidth() * transformedImage.getHeight()];
        for (int y = 0; y < transformedImage.getHeight(); y++) {
            for (int x = 0; x < transformedImage.getWidth(); x++) {
                final Color color = new Color(transformedImage.getRGB(x, y));
                final float[] lab = ColorUtils.rgbToLab(ColorUtils.colorToRgb(color));
                histogramValues[transformedImage.getWidth() * y + x] = lab[LAB_COMPONENTS.indexOf(labComponent)];
            }
        }
        final HistogramDataset histogramDataset = new HistogramDataset();
        histogramDataset.setType(HistogramType.RELATIVE_FREQUENCY);
        histogramDataset.addSeries(join("-", labComponent, "values"), histogramValues, 100);
        final JFreeChart chart = ChartFactory.createHistogram(join("-", labComponent, "Histogram"),
                join("-", labComponent, "value"), "Frequency", histogramDataset,
                PlotOrientation.VERTICAL, false, false, false);
        histogramChartPanel.setChart(chart);
    }

    @Override
    public void handleMouseClicking(final int x, final int y) {
        positionTextFields[0].setText(String.valueOf(x));
        positionTextFields[1].setText(String.valueOf(y));
        final Color color = new Color(transformedImage.getRGB(x, y));
        rgbOutputTextField.setBackground(color);

        final int[] rgb = ColorUtils.colorToRgb(color);
        final float[] hsv = ColorUtils.rgbToHsv(rgb);
        final float[] lab = ColorUtils.rgbToLab(rgb);
        for (int i = 0; i < 3; i++) {
            rgbTextFields[i].setText(String.valueOf(rgb[i]));
            hsvTextFields[i].setText(String.format("%.1f", hsv[i]));
            labTextFields[i].setText(String.format("%.1f", lab[i]));
        }
    }

    @Override
    @SneakyThrows
    public void handleAreaSelection(final int fromX, final int fromY, final int toX, final int toY) {
        final List<Pixel> pixels = new ArrayList<>();
        for (int x = fromX; x <= toX; x++) {
            for (int y = fromY; y <= toY; y++) {
                pixels.add(new Pixel(x, y, new Color(transformedImage.getRGB(x, y))));
            }
        }
        selectedAreaPixelsTextArea.setText(MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(pixels));
    }

    @Override
    @SneakyThrows
    public void handleFileOpening(final File file) {
        changeImage(ImageIO.read(file));
    }

    private void changeImage(final BufferedImage image) {
        reset();
        originalImage = image;
        transformedImage = originalImage;
        updateHistogram();
    }

    private void reset() {
        originalImage = null;
        transformedImage = null;
        for (final JTextField positionTextField : positionTextFields) {
            positionTextField.setText("");
        }
        rgbOutputTextField.setBackground(null);
        for (int i = 0; i < 3; i++) {
            rgbTextFields[i].setText("");
            hsvTextFields[i].setText("");
            labTextFields[i].setText("");
        }
        selectedAreaPixelsTextArea.setText("");
        hueSlider.setValue(0);
        saturationSlider.setValue(0);
        valueSlider.setValue(0);
    }

    @Override
    public List<ImageFeaturesPanelObserver> getObservers() {
        return observers;
    }

    @Override
    public void notifyAboutHsvChanging(final BufferedImage image) {
        observers.forEach(o -> o.handleHsvChanging(image));
    }

    @Override
    public void handleFilterApplying(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleImageSegmentation(final BufferedImage image) {
        changeImage(image);
    }
}
