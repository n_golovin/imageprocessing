package ru.nsu.ccfit.golovin.lab1.ui.component;

import javafx.util.Pair;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.ui.observable.ImagePanelObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observable.ImageSegmentationPanelObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observer.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.lang.StrictMath.max;
import static java.lang.StrictMath.min;
import static javax.imageio.ImageIO.read;

@Slf4j
public class ImagePanel extends JPanel implements ImagePanelObservable, MenuBarObserver, ImageFeaturesPanelObserver,
        ImageFiltersPanelObserver, ImageSegmentationPanelObserver {
    private static final List<ImagePanelObserver> observers = new ArrayList<>();

    private BufferedImage image;
    private JLabel imageLabel;

    public ImagePanel() {
        setLayout(new GridBagLayout());

        addImageLabel();
    }

    @SneakyThrows
    private void addImageLabel() {
        imageLabel = new JLabel();
        imageLabel.addMouseListener(new ImageLabelMouseListener());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.;
        constraints.weighty = 1.;

        add(imageLabel);
    }

    private void reset() {
        image = null;
    }

    private void changeImage(final BufferedImage image) {
        reset();
        this.image = image;
        imageLabel.setIcon(new ImageIcon(image));
    }

    @Override
    @SneakyThrows
    public void handleFileOpening(final File file) {
        changeImage(read(file));
    }

    @Override
    public List<ImagePanelObserver> getObservers() {
        return observers;
    }

    @Override
    public void notifyAboutMouseClicking(final int x, final int y) {
        observers.forEach(o -> o.handleMouseClicking(x, y));
    }

    @Override
    public void notifyAboutAreaSelection(final int fromX, final int fromY, final int toX, final int toY) {
        observers.forEach(o -> o.handleAreaSelection(fromX, fromY, toX, toY));
    }

    @Override
    public void handleHsvChanging(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleFilterApplying(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleImageSegmentation(final BufferedImage image) {
        changeImage(image);
    }

    private class ImageLabelMouseListener extends MouseAdapter {
        private int mousePressedX;
        private int mousePressedY;

        @Override
        public void mouseClicked(final MouseEvent e) {
            if (image == null) {
                return;
            }
            notifyAboutMouseClicking(e.getX(), e.getY());
        }

        @Override
        public void mousePressed(final MouseEvent e) {
            if (image == null) {
                return;
            }
            mousePressedX = e.getX();
            mousePressedY = e.getY();
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
            if (e.getX() == mousePressedX && e.getY() == mousePressedY || image == null) {
                return;
            }
            final Pair<Integer, Integer> xMinMax = getMinMax(mousePressedX, e.getX(), 0, image.getWidth() - 1);
            final Pair<Integer, Integer> yMinMax = getMinMax(mousePressedY, e.getY(), 0, image.getHeight() - 1);
            notifyAboutAreaSelection(xMinMax.getKey(), yMinMax.getKey(), xMinMax.getValue(), yMinMax.getValue());
        }

        private Pair<Integer, Integer> getMinMax(final int a, final int b, final int lowerBound, final int upperBound) {
            final int min = min(a, b);
            final int max = max(a, b);
            return new Pair<>(min > lowerBound ? min : lowerBound, max < upperBound ? max : upperBound);
        }
    }
}
