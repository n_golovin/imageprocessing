package ru.nsu.ccfit.golovin.lab1.bl.filter.impl;

import lombok.NonNull;
import ru.nsu.ccfit.golovin.lab1.util.ColorUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.round;
import static java.lang.StrictMath.sqrt;

public class SobelOperator extends AbstractFilter {
    private static final double[][] HORIZONTAL_KERNEL = {
            {1., 2., 1.},
            {0., 0., 0.},
            {-1., -2., -1.}
    };
    private static final double[][] VERTICAL_KERNEL = {
            {-1., 0., 1.},
            {-2., 0., 2.},
            {-1., 0., 1.}
    };

    @Override
    double[][] getKernel() {
        return null;
    }

    @Override
    @NonNull
    int[] filter(final BufferedImage image, final int x, final int y) {
        final int kernelSize = HORIZONTAL_KERNEL.length;
        final int mean = kernelSize / 2;
        final int[] horizontalSobelRgb = {0, 0, 0};
        final int[] verticalSobelRgb = {0, 0, 0};
        for (int i = 0; i < kernelSize; i++) {
            for (int j = 0; j < kernelSize; j++) {
                final int currentX = x + (i - mean);
                final int currentY = y + (j - mean);
                if (currentX < 0 || currentX >= image.getWidth() || currentY < 0 || currentY >= image.getHeight()) {
                    continue;
                }
                final Color color = new Color(image.getRGB(currentX, currentY));
                final int[] rgb = ColorUtils.colorToRgb(color);
                for (int k = 0; k < rgb.length; k++) {
                    horizontalSobelRgb[k] += HORIZONTAL_KERNEL[i][j] * rgb[k];
                    verticalSobelRgb[k] += VERTICAL_KERNEL[i][j] * rgb[k];
                }
            }
        }
        final int[] filteredRgb = {0, 0, 0};
        for (int i = 0; i < filteredRgb.length; i++) {
            filteredRgb[i] = (int) round(sqrt(pow(horizontalSobelRgb[i], 2) + pow(verticalSobelRgb[i], 2)));
        }
        return filteredRgb;
    }
}
