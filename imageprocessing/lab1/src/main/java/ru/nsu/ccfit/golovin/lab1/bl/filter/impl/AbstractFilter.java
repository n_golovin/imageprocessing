package ru.nsu.ccfit.golovin.lab1.bl.filter.impl;

import lombok.NonNull;
import ru.nsu.ccfit.golovin.lab1.bl.filter.ImageFilter;
import ru.nsu.ccfit.golovin.lab1.util.ColorUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class AbstractFilter implements ImageFilter {
    abstract double[][] getKernel();

    @NonNull
    int[] filter(final BufferedImage image, final int x, final int y) {
        final double[][] kernel = getKernel();
        final int kernelSize = kernel.length;
        final int mean = kernelSize / 2;
        final int[] filteredRgb = {0, 0, 0};
        for (int i = 0; i < kernelSize; i++) {
            for (int j = 0; j < kernelSize; j++) {
                final int currentX = x + (i - mean);
                final int currentY = y + (j - mean);
                if (currentX < 0 || currentX >= image.getWidth() || currentY < 0 || currentY >= image.getHeight()) {
                    continue;
                }
                final Color color = new Color(image.getRGB(currentX, currentY));
                final int[] rgb = ColorUtils.colorToRgb(color);
                for (int k = 0; k < rgb.length; k++) {
                    filteredRgb[k] += kernel[i][j] * rgb[k];
                }
            }
        }
        return filteredRgb;
    }

    @Override
    @NonNull
    public BufferedImage applyFilter(final BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();
        final BufferedImage filteredImage = new BufferedImage(w, h, image.getType());
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                final int[] filteredRgb = filter(image, x, y);
                final int[] normalizedRgb = normalizeRgb(filteredRgb);
                final Color filteredColor = new Color(normalizedRgb[0], normalizedRgb[1], normalizedRgb[2]);
                filteredImage.setRGB(x, y, filteredColor.getRGB());
            }
        }
        return filteredImage;
    }

    @NonNull
    private static int[] normalizeRgb(final int[] rgb) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final int[] normalizedRgb = new int[rgb.length];
        for (int i = 0; i < rgb.length; i++) {
            normalizedRgb[i] = rgb[i] > 0 ? (rgb[i] < 255 ? rgb[i] : 255) : 0;
        }
        return normalizedRgb;
    }
}
