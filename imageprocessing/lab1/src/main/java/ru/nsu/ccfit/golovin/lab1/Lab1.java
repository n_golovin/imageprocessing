package ru.nsu.ccfit.golovin.lab1;

import ru.nsu.ccfit.golovin.lab1.ui.component.MainFrame;

public class Lab1 {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> new MainFrame().setVisible(true));
    }
}
