package ru.nsu.ccfit.golovin.lab1.util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class ImageUtils {
    @NonNull
    public static void save(final BufferedImage image, final String format, final String filename) {
        try {
            ImageIO.write(image, format, new File(filename + "." + format));
        } catch (IOException e) {
            log.warn("", e);
        }
    }
}
