package ru.nsu.ccfit.golovin.lab1.ui.component;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

@Slf4j
public class MainFrame extends JFrame {
    private MenuBar menuBar;
    private ImagePanel imagePanel;
    private ImageFeaturesPanel imageFeaturesPanel;
    private ImageFiltersPanel imageFiltersPanel;
    private ImageSegmentationPanel imageSegmentationPanel;

    public MainFrame() {
        setTitle("Lab1");
        setLayout(new GridBagLayout());
        setMinimumSize(new Dimension( 1680, 1050));
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        addMenuBar();
        addImagePanel();
        addTabbedPane();
        wireComponents();
    }

    private void wireComponents() {
        menuBar.addObserver(imagePanel);
        menuBar.addObserver(imageFeaturesPanel);
        menuBar.addObserver(imageFiltersPanel);
        menuBar.addObserver(imageSegmentationPanel);

        imagePanel.addObserver(imageFeaturesPanel);

        imageFeaturesPanel.addObserver(imagePanel);
        imageFeaturesPanel.addObserver(imageFiltersPanel);
        imageFeaturesPanel.addObserver(imageSegmentationPanel);
        imageFeaturesPanel.addObserver(menuBar);

        imageFiltersPanel.addObserver(imagePanel);
        imageFiltersPanel.addObserver(imageFeaturesPanel);
        imageFiltersPanel.addObserver(imageSegmentationPanel);
        imageFiltersPanel.addObserver(menuBar);

        imageSegmentationPanel.addObserver(imagePanel);
        imageSegmentationPanel.addObserver(imageFeaturesPanel);
        imageSegmentationPanel.addObserver(imageFiltersPanel);
        imageSegmentationPanel.addObserver(menuBar);
    }

    private void addImagePanel() {
        imagePanel = new ImagePanel();

        final JScrollPane imagePanelScrollPane = new JScrollPane(imagePanel);
        imagePanelScrollPane.setBorder(BorderFactory.createTitledBorder("Image panel"));
        imagePanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.;
        constraints.weighty = 1.;

        add(imagePanelScrollPane, constraints);
    }

    private void addMenuBar() {
        menuBar = new MenuBar();
        setJMenuBar(menuBar);
    }

    private void addTabbedPane() {
        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Lab1", createImageFeaturesPanel());
        tabbedPane.addTab("Lab2", createImageFiltersPanel());
        tabbedPane.addTab("Lab3", createImageSegmentationPanel());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1.e-2;
        constraints.weighty = 1.0;

        add(tabbedPane, constraints);
    }

    private Component createImageFiltersPanel() {
        imageFiltersPanel = new ImageFiltersPanel();
        return imageFiltersPanel;
    }

    private Component createImageFeaturesPanel() {
        imageFeaturesPanel = new ImageFeaturesPanel();
        return imageFeaturesPanel;
    }

    private Component createImageSegmentationPanel() {
        imageSegmentationPanel = new ImageSegmentationPanel();
        return imageSegmentationPanel;
    }
}
