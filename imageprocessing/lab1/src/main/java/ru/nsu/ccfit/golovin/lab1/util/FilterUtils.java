package ru.nsu.ccfit.golovin.lab1.util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.bl.filter.impl.GaborFilter;
import ru.nsu.ccfit.golovin.lab1.bl.filter.impl.GaussianBlur;
import ru.nsu.ccfit.golovin.lab1.bl.filter.impl.SobelOperator;

import java.awt.image.BufferedImage;

@Slf4j
public class FilterUtils {
    @NonNull
    public static BufferedImage gaussianBlur(final BufferedImage image, final int sigma) {
        return new GaussianBlur(sigma).applyFilter(image);
    }

    @NonNull
    public static BufferedImage sobelOperator(final BufferedImage image) {
        return new SobelOperator().applyFilter(image);
    }

    @NonNull
    public static BufferedImage gaborFilter(final BufferedImage image, final int orientation) {
        return new GaborFilter(orientation).applyFilter(image);
    }
}
