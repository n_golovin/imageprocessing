package ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.ImageSegmenter;
import ru.nsu.ccfit.golovin.lab1.util.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.lang.Math.round;
import static java.lang.StrictMath.exp;
import static java.lang.StrictMath.pow;
import static ru.nsu.ccfit.golovin.lab1.util.ColorUtils.*;

@Slf4j
public class MeanShift implements ImageSegmenter {
    private static final int SEGMENTATION_RADIUS = 5;
    private static final int ITERATION_NUMBERS = 15;

    @Override
    @NonNull
    public BufferedImage applySegmentation(final BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();
        BufferedImage originalImage = new BufferedImage(w,  h, image.getType());
        originalImage.setData(image.getData());
        BufferedImage segmentedImage = null;
        for (int n = 0; n < ITERATION_NUMBERS; n++) {
            segmentedImage = new BufferedImage(w, h, image.getType());
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    final Color shiftedColor = computeShiftedColor(originalImage, x, y);
                    segmentedImage.setRGB(x, y, shiftedColor.getRGB());
                }
            }
            originalImage = segmentedImage;
            ImageUtils.save(segmentedImage, "jpg", "mean_shift_" + (n + 1));
        }
        return segmentedImage;
    }

    @NonNull
    private Color computeShiftedColor(final BufferedImage originalImage, final int x, final int y) {
        final Color originalColor = new Color(originalImage.getRGB(x, y));
        final float[] originalLab = colorToLab(originalColor);
        double weightSum = 0.;
        final int[] shiftedRgb = new int[3];
        for (int i = 0; i < 2 * SEGMENTATION_RADIUS; i++) {
            for (int j = 0; j < 2 * SEGMENTATION_RADIUS; j++) {
                final int currentX = x + (i - SEGMENTATION_RADIUS);
                final int currentY = y + (j - SEGMENTATION_RADIUS);
                if (currentX < 0 || currentX >= originalImage.getWidth() || currentY < 0 || currentY >= originalImage.getHeight()) {
                    continue;
                }
                final int[] currentRgb = colorToRgb(new Color(originalImage.getRGB(currentX, currentY)));
                final float[] currentLab = rgbToLab(currentRgb);
                final double distance = computeDistanceBetweenColors(originalLab, currentLab);
                final double weight = computeColorWeight(distance);
                for (int k = 0; k < shiftedRgb.length; k++) {
                    shiftedRgb[k] += weight * currentRgb[k];
                }
                weightSum += weight;
            }
        }
        return new Color(
                (int) round(shiftedRgb[0] / weightSum),
                (int) round(shiftedRgb[1] / weightSum),
                (int) round(shiftedRgb[2] / weightSum));
    }

    private static double computeColorWeight(final double distance){
        return exp(-0.5 * pow(distance / SEGMENTATION_RADIUS, 2.));
    }
}

