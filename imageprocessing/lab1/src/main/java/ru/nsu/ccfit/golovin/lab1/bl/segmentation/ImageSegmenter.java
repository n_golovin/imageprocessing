package ru.nsu.ccfit.golovin.lab1.bl.segmentation;

import java.awt.image.BufferedImage;

public interface ImageSegmenter {
    BufferedImage applySegmentation(BufferedImage image);
}
