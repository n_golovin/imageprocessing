package ru.nsu.ccfit.golovin.lab1.ui.observable;

import ru.nsu.ccfit.golovin.lab1.ui.observer.ImagePanelObserver;

public interface ImagePanelObservable extends UIObservable<ImagePanelObserver> {
    void notifyAboutMouseClicking(int x, int y);

    void notifyAboutAreaSelection(int fromX, int fromY, int toX, int toY);
}
