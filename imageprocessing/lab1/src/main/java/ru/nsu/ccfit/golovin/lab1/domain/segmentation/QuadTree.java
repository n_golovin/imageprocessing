package ru.nsu.ccfit.golovin.lab1.domain.segmentation;

import lombok.Data;

@Data
public class QuadTree {
    private final ColorArea colorArea;
    private QuadTree[] children;
}
