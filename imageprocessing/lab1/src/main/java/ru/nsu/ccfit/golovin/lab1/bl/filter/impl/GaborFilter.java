package ru.nsu.ccfit.golovin.lab1.bl.filter.impl;

import static java.lang.StrictMath.*;

public class GaborFilter extends AbstractFilter {
    private static final double WAVE_LENGTH = 2.;
    private static final double ASPECT_RATIO = 1.;
    private static final int PHASE_OFFSET = 0;
    private static final int KERNEL_SIZE = 5;

    private final int orientation;

    public GaborFilter(final int orientation) {
        this.orientation = orientation;
    }

    @Override
    double[][] getKernel() {
        final double sigma = 0.56 * WAVE_LENGTH;
        final double[][] kernel = new double[KERNEL_SIZE][KERNEL_SIZE];
        final int mean = KERNEL_SIZE / 2;
        for (int i = 0; i < KERNEL_SIZE; i++) {
            for (int j = 0; j < KERNEL_SIZE; j++) {
                final double orientationRadians = toRadians(orientation);
                final double x = (i - mean) * cos(orientationRadians) + (j - mean) * sin(orientationRadians);
                final double y = -(i - mean) * sin(orientationRadians) + (j - mean) * cos(orientationRadians);
                kernel[i][j] = exp(-(pow(x / sigma, 2) + pow(y * ASPECT_RATIO / sigma, 2)) / 2) *
                        cos(2 * PI * x / WAVE_LENGTH + toRadians(PHASE_OFFSET));
            }
        }
        return kernel;
    }
}
