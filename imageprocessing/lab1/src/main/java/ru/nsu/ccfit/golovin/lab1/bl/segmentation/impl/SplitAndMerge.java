package ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl;

import lombok.NonNull;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.ImageSegmenter;
import ru.nsu.ccfit.golovin.lab1.domain.segmentation.ColorArea;
import ru.nsu.ccfit.golovin.lab1.domain.segmentation.QuadTree;
import ru.nsu.ccfit.golovin.lab1.util.ImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.round;
import static ru.nsu.ccfit.golovin.lab1.util.ColorUtils.*;

public class SplitAndMerge implements ImageSegmenter {
    private static final double SIMILAR_COLOR_DISTANCE = 5.;
    private static final int MIN_AREA_SIZE = 16;

    @Override
    @NonNull
    public BufferedImage applySegmentation(final BufferedImage image) {
        final int w = image.getWidth();
        final int h = image.getHeight();
        final QuadTree root = new QuadTree(new ColorArea(new Rectangle(0, 0, w, h)));
        final List<ColorArea> colorAreas = new ArrayList<>();
        split(image, root, colorAreas);
        ImageUtils.save(fillImage(new BufferedImage(w, h, image.getType()), colorAreas), "jpg", "split");
        merge(colorAreas);
        final BufferedImage segmentedImage = fillImage(new BufferedImage(w, h, image.getType()), colorAreas);
        ImageUtils.save(segmentedImage, "jpg", "merge");
        return segmentedImage;
    }

    @NonNull
    private static void split(final BufferedImage image, final QuadTree quadTree, final List<ColorArea> colorAreas) {
        final Rectangle area = quadTree.getColorArea().getArea();
        final float[] initialLab = colorToLab(new Color(image.getRGB(area.x, area.y)));
        final int[] meanRgb = new int[3];
        double colorDeviation = 0;
        for (int y = area.y; y < area.y + area.height; y++) {
            for (int x = area.x; x < area.x + area.width; x++) {
                final int[] rgb = colorToRgb(new Color(image.getRGB(x, y)));
                for (int i = 0; i < meanRgb.length; i++) {
                    meanRgb[i] += rgb[i];
                }
                colorDeviation += computeDistanceBetweenColors(initialLab, rgbToLab(rgb));
            }
        }
        colorDeviation /= area.width * area.height;
        if (colorDeviation <= SIMILAR_COLOR_DISTANCE || area.width <= MIN_AREA_SIZE || area.height <= MIN_AREA_SIZE) {
            for (int i = 0; i < meanRgb.length; i++) {
                meanRgb[i] = round((float) meanRgb[i] / area.width / area.height);
            }
            final Color color = new Color(meanRgb[0], meanRgb[1], meanRgb[2]);
            quadTree.getColorArea().setColor(color);
            colorAreas.add(quadTree.getColorArea());
        }
        else {
            final float halfWidth = area.width / 2.F;
            final float halfHeight = area.height / 2.F;
            quadTree.setChildren(new QuadTree[]{
                    new QuadTree(new ColorArea(new Rectangle(area.x, area.y, (int) halfWidth, (int) halfHeight))),
                    new QuadTree(new ColorArea(new Rectangle((int) (area.x + halfWidth), area.y, round(halfWidth), (int) halfHeight))),
                    new QuadTree(new ColorArea(new Rectangle(area.x, (int) (area.y + halfHeight), (int) halfWidth, round(halfHeight)))),
                    new QuadTree(new ColorArea(new Rectangle((int) (area.x + halfWidth), (int) (area.y + halfHeight), round(halfWidth), round(halfHeight))))
            });
            for (QuadTree child : quadTree.getChildren()) {
                split(image, child, colorAreas);
            }
        }
    }

    @NonNull
    private static void merge(final List<ColorArea> colorAreas) {
        boolean hasMerged = true;
        while (hasMerged) {
            hasMerged = false;
            for (ColorArea firstColorArea : colorAreas) {
                for (ColorArea secondColorArea : colorAreas) {
                    if (firstColorArea == secondColorArea) {
                        continue;
                    }
                    final Rectangle intersection = firstColorArea.getArea().intersection(secondColorArea.getArea());
                    if (intersection.width != 0 && intersection.height != 0 || intersection.width < 0 || intersection.height < 0) {
                        continue;
                    }
                    final Color firstColor = firstColorArea.getColor();
                    final Color secondColor = secondColorArea.getColor();
                    if (firstColor.equals(secondColor)) {
                        continue;
                    }
                    if (computeDistanceBetweenColors(colorToLab(firstColor), colorToLab(secondColor)) <= SIMILAR_COLOR_DISTANCE) {
                        final Color mergedColor = new Color(
                                round(((float) firstColor.getRed() + secondColor.getRed()) / 2),
                                round(((float) firstColor.getGreen() + secondColor.getGreen()) / 2),
                                round(((float) firstColor.getBlue() + secondColor.getBlue()) / 2)
                        );
                        firstColorArea.setColor(mergedColor);
                        secondColorArea.setColor(mergedColor);
                        hasMerged = true;
                        break;
                    }
                }
            }
        }
    }

    @NonNull
    private static BufferedImage fillImage(final BufferedImage image, final List<ColorArea> colorAreas) {
        colorAreas.forEach(colorArea -> {
            final Rectangle area = colorArea.getArea();
            for (int y = area.y; y < area.y + area.height; y++) {
                for (int x = area.x; x < area.x + area.width; x++) {
                    image.setRGB(x, y, colorArea.getColor().getRGB());
                }
            }
        });
        return image;
    }
}
