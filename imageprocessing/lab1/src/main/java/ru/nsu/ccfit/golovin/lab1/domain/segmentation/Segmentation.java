package ru.nsu.ccfit.golovin.lab1.domain.segmentation;

import ru.nsu.ccfit.golovin.lab1.bl.segmentation.ImageSegmenter;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl.MeanShift;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl.NormalizedCuts;
import ru.nsu.ccfit.golovin.lab1.bl.segmentation.impl.SplitAndMerge;

import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public enum Segmentation {
    SPLIT_AND_MERGE("Split and Merge", new SplitAndMerge()),
    NORMALIZED_CUTS("Normalized Cuts", new NormalizedCuts()),
    MEAN_SHIFT("Mean Shift", new MeanShift());

    private static final Map<String, Segmentation> stringToEnum = Stream.of(values()).collect(toMap(Object::toString, identity()));
    private final String prettyName;
    private final ImageSegmenter imageSegmenter;

    Segmentation(final String prettyName, final ImageSegmenter imageSegmenter) {
        this.prettyName = prettyName;
        this.imageSegmenter = imageSegmenter;
    }

    public BufferedImage applySegmentation(final BufferedImage image) {
        return imageSegmenter.applySegmentation(image);
    }

    @Override
    public String toString() {
        return prettyName;
    }

    public static Optional<Segmentation> fromString(final String string) {
        return Optional.ofNullable(stringToEnum.get(string));
    }
}
