package ru.nsu.ccfit.golovin.lab1.ui.observable;

import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFiltersPanelObserver;

import java.awt.image.BufferedImage;

public interface ImageFiltersPanelObservable extends UIObservable<ImageFiltersPanelObserver> {
    void notifyAboutFilterApplying(BufferedImage image);
}
