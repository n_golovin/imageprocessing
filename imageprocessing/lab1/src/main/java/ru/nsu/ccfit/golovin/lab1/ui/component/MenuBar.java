package ru.nsu.ccfit.golovin.lab1.ui.component;

import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.ui.observable.MenuBarObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFeaturesPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFiltersPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageSegmentationPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.MenuBarObserver;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.JOptionPane.showMessageDialog;
import static org.apache.commons.io.FilenameUtils.getExtension;

@Slf4j
public class MenuBar extends JMenuBar implements MenuBarObservable, ImageFeaturesPanelObserver, ImageFiltersPanelObserver,
        ImageSegmentationPanelObserver {
    private static final Font VERDANA_FONT = new Font("Verdana", Font.PLAIN, 12);
    private static final List<MenuBarObserver> observers = new ArrayList<>();

    private JMenu fileMenu;
    private JMenuItem openMenu;
    private JMenuItem saveMenu;
    private JMenuItem exitMenu;
    private JFileChooser fileChooser;
    private BufferedImage image;

    public MenuBar() {
        add(createFileMenu());
    }

    private JMenu createFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setFont(VERDANA_FONT);
        createFileChooser();
        fileMenu.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(final MenuEvent e) {
                if (image == null) {
                    saveMenu.setEnabled(false);
                } else {
                    saveMenu.setEnabled(true);
                }
            }

            @Override
            public void menuDeselected(final MenuEvent e) {
            }

            @Override
            public void menuCanceled(final MenuEvent e) {
            }
        });

        fileMenu.add(createOpenMenu());
        fileMenu.add(createSaveMenu());
        fileMenu.addSeparator();
        fileMenu.add(createExitMenu());

        return fileMenu;
    }

    private void createFileChooser() {
        fileChooser = new JFileChooser(getClass().getClassLoader().getResource("images").getPath());
        fileChooser.setFileFilter(new FileNameExtensionFilter("Images", "jpg", "png"));
    }

    private JMenuItem createExitMenu() {
        exitMenu = new JMenuItem("Exit");
        exitMenu.setFont(VERDANA_FONT);
        exitMenu.addActionListener(e -> System.exit(0));
        return exitMenu;
    }

    private JMenuItem createOpenMenu() {
        openMenu = new JMenuItem("Open");
        openMenu.setFont(VERDANA_FONT);
        openMenu.addActionListener(e -> {
            int actionStatus = fileChooser.showOpenDialog(this);
            if (actionStatus == JFileChooser.APPROVE_OPTION) {
                try {
                    image = ImageIO.read(fileChooser.getSelectedFile());
                    new ImageIcon(image);
                    notifyAboutFileOpening(fileChooser.getSelectedFile());
                } catch (Exception ex) {
                    log.warn("Failed to open file {}", fileChooser.getSelectedFile().getAbsolutePath());
                    showMessageDialog(getParent(),
                            "Failed to open selected file",
                            "Opening file error",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return openMenu;
    }

    private JMenuItem createSaveMenu() {
        saveMenu = new JMenuItem("Save");
        saveMenu.setFont(VERDANA_FONT);
        saveMenu.addActionListener(e -> {
            if (image == null) {
                return;
            }
            int actionStatus = fileChooser.showSaveDialog(this);
            if (actionStatus == JFileChooser.APPROVE_OPTION) {
                try {
                    ImageIO.write(image, getExtension(fileChooser.getSelectedFile().getName()), fileChooser.getSelectedFile());
                } catch (Exception ex) {
                    log.warn("Failed to save file {}", fileChooser.getSelectedFile().getAbsolutePath());
                    showMessageDialog(getParent(),
                            "Failed to save selected file",
                            "Saving file error",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return saveMenu;
    }

    private void changeImage(final BufferedImage image) {
        this.image = image;
    }

    @Override
    public List<MenuBarObserver> getObservers() {
        return observers;
    }

    @Override
    public void notifyAboutFileOpening(final File file) {
        observers.forEach(o -> o.handleFileOpening(file));
    }

    @Override
    public void handleHsvChanging(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleFilterApplying(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleImageSegmentation(final BufferedImage image) {
        changeImage(image);
    }
}
