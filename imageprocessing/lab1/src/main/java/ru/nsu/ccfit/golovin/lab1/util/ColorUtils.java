package ru.nsu.ccfit.golovin.lab1.util;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.List;

import static java.lang.StrictMath.*;
import static java.util.Arrays.asList;

@Slf4j
public class ColorUtils {
    private static final double[][] M = {
            {0.4124564, 0.3575761, 0.1804375},
            {0.2126729, 0.7151522, 0.0721750},
            {0.0193339, 0.1191920, 0.9503041}
    };
    private static final double[][] TRANSFORM_HSV_R = {
            {0.299 , 0.701, 0.168},
            {0.587, -0.587, 0.330},
            {0.114, -0.114, -0.497}
    };
    private static final double[][] TRANSFORM_HSV_G = {
            {0.299, -0.299, -0.328},
            {0.587, 0.413, 0.035},
            {0.114, -0.114, 0.292}
    };
    private static final double[][] TRANSFORM_HSV_B = {
            {0.299 , -0.3, 1.25},
            {0.587, -0.588, -1.05},
            {0.114, 0.886, -0.203}
    };
    private static final double XN = 95.047;
    private static final double YN = 100.;
    private static final double ZN = 108.883;

    @NonNull
    public static int[] colorToRgb(final Color color) {
        return new int[]{color.getRed(), color.getGreen(), color.getBlue()};
    }

    @NonNull
    public static float[] colorToLab(final Color color) {
        return rgbToLab(colorToRgb(color));
    }

    @NonNull
    public static float[] rgbToHsv(final int[] rgb) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final float[] hsv = new float[3];
        Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsv);
        hsv[0] *= 360;
        hsv[1] *= 100;
        hsv[2] *= 100;
        return hsv;
    }

    @NonNull
    public static float[] rgbToLab(final int[] rgb) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final float[] xyz = rgbToXyz(rgb);
        final double l = f(xyz[1] / YN);
        final double L = 116. * l - 16.;
        final double a = 500. * (f(xyz[0] / XN) - l);
        final double b = 200. * (l - f(xyz[2] / ZN));
        return new float[] {(float) L, (float) a, (float) b};
    }

    @NonNull
    private static float[] rgbToXyz(final int[] rgb) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final float[] linearRgb = rgbToLinearRgb(rgb);
        final double[] xyz = {0.,0.,0.};
        for (int i = 0; i < xyz.length; i++) {
            for (int j = 0; j < linearRgb.length; j++) {
                xyz[i] += M[i][j] * linearRgb[j];
            }
        }
        return new float[]{(float) xyz[0], (float) xyz[1], (float) xyz[2]};
    }

    @NonNull
    private static float[] rgbToLinearRgb(final int[] rgb) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final double[] linearRgb = {rgb[0]/255., rgb[1]/255., rgb[2]/255.};
        for (int i = 0; i < linearRgb.length; i++) {
            if (linearRgb[i] > 0.04045) {
                linearRgb[i] = pow((linearRgb[i] + 0.055) / 1.055, 2.4);
            } else {
                linearRgb[i] = linearRgb[i] / 12.92;
            }
            linearRgb[i] *= 100.;
        }
        return new float[]{(float) linearRgb[0], (float) linearRgb[1], (float) linearRgb[2]};
    }

    @NonNull
    public static BufferedImage unnormalizedTransformImageHSV(final BufferedImage image, final int h, final int s, final int v) {
        final BufferedImage transformedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                final Color color = new Color(image.getRGB(x, y));
                final float[] hsv = rgbToHsv(colorToRgb(color));
                hsv[0] += h;
                if (hsv[0] > 360.) {
                    hsv[0] -= 360.;
                } else if (hsv[0] < 0.) {
                    hsv[0] += 360.;
                }
                hsv[1] += s;
                if (hsv[1] > 100.) {
                    hsv[1] = 100.f;
                } else if (hsv[1] < 0.) {
                    hsv[1] = 0.f;
                }
                hsv[2] += v;
                if (hsv[2] > 100.) {
                    hsv[2] = 100.f;
                } else if (hsv[2] < 0.) {
                    hsv[2] = 0.f;
                }
                final Color tranformedColor = Color.getHSBColor(hsv[0] / 360.f, hsv[1] / 100.f, hsv[2] / 100.f);
                transformedImage.setRGB(x, y, tranformedColor.getRGB());
            }
        }
        return transformedImage;
    }

    @NonNull
    public static BufferedImage normalizedTransformImageHSV(final BufferedImage image, final float h, final float s, final float v) {
        final double[] normalizationCoefficients = computeNormalizationCoefficients(image, h, s, v);
        final BufferedImage transformedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                final Color color = new Color(image.getRGB(x, y));
                final double[] transformedRgb = computeTransformedRgb(color, h, s, v);
                final int[] normalizedRgb = normalizeRgb(transformedRgb, normalizationCoefficients[0], normalizationCoefficients[1]);
                final Color normalizedColor = new Color(normalizedRgb[0], normalizedRgb[1], normalizedRgb[2]);
                transformedImage.setRGB(x, y, normalizedColor.getRGB());
            }
        }
        return transformedImage;
    }

    @NonNull
    private static double[] computeNormalizationCoefficients(final BufferedImage image, final float h, final float s, final float v) {
        final double[] normalizationCoefficients = {0., 255.};
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                final Color color = new Color(image.getRGB(x, y));
                final double[] transformedRgb = computeTransformedRgb(color, h, s, v);
                final List<Double> possibleNormalizationCoefficients = asList(transformedRgb[0], transformedRgb[1], transformedRgb[2],
                        normalizationCoefficients[0], normalizationCoefficients[1]);
                normalizationCoefficients[0] = Collections.min(possibleNormalizationCoefficients);
                normalizationCoefficients[1] = Collections.max(possibleNormalizationCoefficients);
            }
        }
        return normalizationCoefficients;
    }

    @NonNull
    private static double[] computeTransformedRgb(final Color color, final float h, final float s, final float v) {
        final double vsu = v * s * cos(toRadians(h));
        final double vsw = s * s * sin(toRadians(h));
        final int[] rgb = colorToRgb(color);
        final double[] transformedRgb = {0., 0., 0.};
        for (int i = 0; i < rgb.length; i++) {
            transformedRgb[0] += (TRANSFORM_HSV_R[i][0] * v + TRANSFORM_HSV_R[i][1] * vsu + TRANSFORM_HSV_R[i][2] * vsw) * rgb[i];
            transformedRgb[1] += (TRANSFORM_HSV_G[i][0] * v + TRANSFORM_HSV_G[i][1] * vsu + TRANSFORM_HSV_G[i][2] * vsw) * rgb[i];
            transformedRgb[2] += (TRANSFORM_HSV_B[i][0] * v + TRANSFORM_HSV_B[i][1] * vsu + TRANSFORM_HSV_B[i][2] * vsw) * rgb[i];
        }
        return transformedRgb;
    }

    @NonNull
    private static int[] normalizeRgb(final double[] rgb, final double minValue, final double maxValue) {
        if (rgb.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Rgb length expected: 3, actual: " + rgb.length);
        }
        final int[] normalizedRgb = new int[3];
        for (int i = 0; i < rgb.length; i++) {
            normalizedRgb[i] = (int) round((rgb[i] + abs(minValue)) / (maxValue + abs(minValue)) * 255.);
        }
        return normalizedRgb;
    }

    private static double f(final double x) {
        if (x > 216. / 24389.) {
            return cbrt(x);
        } else {
            return 841. / 108. * x + 4. / 29.;
        }
    }

    @NonNull
    public static double computeDistanceBetweenColors(final float[] lab1, final float[] lab2) {
        if (lab1.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Lab1 length expected: 3, actual: " + lab1.length);
        }
        if (lab2.length != 3) {
            throw new ArrayIndexOutOfBoundsException("Lab2 length expected: 3, actual: " + lab2.length);
        }
        return computeDistanceBetweenColors(lab1[0], lab1[1], lab1[2], lab2[0], lab2[1], lab2[2]);
    }

    private static double computeDistanceBetweenColors(final float L1, final float a1, final float b1,
                                                       final float L2, final float a2, final float b2) {
        final double deltaLprime = L2 - L1;

        final double LMean = (L1 + L2) / 2.;
        final double C1 = sqrt(a1 * a1 + b1 * b1);
        final double C2 = sqrt(a2 * a2 + b2 * b2);
        final double CMean = (C1 + C2) / 2.;

        final double G = (1. - sqrt(pow(CMean, 7.) / (pow(CMean, 7.) + pow(25., 7.)))) / 2.;
        final double a1Prime = a1 * (1. + G);
        final double a2Prime = a2 * (1. + G);

        final double C1Prime = sqrt(a1Prime * a1Prime + b1 * b1);
        final double C2Prime = sqrt(a2Prime * a2Prime + b2 * b2);
        final double CMeanPrime = (C1Prime + C2Prime) / 2;
        final double deltaCprime = C2Prime - C1Prime;

        final double h1Prime = atan2(b1, a1Prime) + 2. * PI * (atan2(b1, a1Prime) < 0. ? 1. : 0.);
        final double h2Prime = atan2(b2, a2Prime) + 2. * PI * (atan2(b2, a2Prime) < 0. ? 1. : 0.);

        final double deltahPrime = (abs(h1Prime - h2Prime) <= PI) ?
                h2Prime - h1Prime : (h2Prime <= h1Prime) ?
                h2Prime - h1Prime + 2. * PI : h2Prime - h1Prime - 2 * PI;

        final double deltaHPrime = 2. * sqrt(C1Prime * C2Prime) * sin(deltahPrime / 2.);
        final double HMeanPrime = (abs(h1Prime - h2Prime) > PI) ? (h1Prime + h2Prime + 2 * PI) / 2 : (h1Prime + h2Prime) / 2.;

        final double T = 1. - 0.17 * cos(HMeanPrime - PI / 6.) +
                0.24 * cos(2. * HMeanPrime) +
                0.32 * cos(3. * HMeanPrime + PI / 30.) -
                0.2 * cos(4. * HMeanPrime - 7. * PI / 20.);

        final double SL = 1. + ((0.015 * pow(LMean - 50., 2.)) / (sqrt(20. + pow(LMean - 50., 2.))));
        final double SC = 1. + 0.045 * CMeanPrime;
        final double SH = 1. + 0.015 * CMeanPrime * T;

        final double RC = 2. * sqrt(pow(CMeanPrime, 7.) / (pow(CMeanPrime, 7.) + pow(25., 7.)));
        final double deltaTheta = PI / 6 * exp(-pow((toDegrees(HMeanPrime) - 275.) / 25., 2));
        final double RT = -RC * sin(2 * deltaTheta);

        final double kL = 1.;
        final double kC = 1.;
        final double kH = 1.;
        return sqrt(((deltaLprime / (kL * SL)) * (deltaLprime / (kL * SL))) +
                ((deltaCprime / (kC * SC)) * (deltaCprime / (kC * SC))) +
                ((deltaHPrime / (kH * SH)) * (deltaHPrime / (kH * SH))) +
                (RT * (deltaCprime / (kC * SC)) * (deltaHPrime / (kH * SH)))
        );
    }
}
