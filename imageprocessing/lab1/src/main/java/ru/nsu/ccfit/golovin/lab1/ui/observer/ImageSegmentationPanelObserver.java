package ru.nsu.ccfit.golovin.lab1.ui.observer;

import java.awt.image.BufferedImage;

public interface ImageSegmentationPanelObserver {
    void handleImageSegmentation(BufferedImage image);
}
