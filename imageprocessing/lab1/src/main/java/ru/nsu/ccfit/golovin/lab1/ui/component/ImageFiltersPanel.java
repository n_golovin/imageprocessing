package ru.nsu.ccfit.golovin.lab1.ui.component;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.domain.filter.Filter;
import ru.nsu.ccfit.golovin.lab1.ui.observable.ImageFiltersPanelObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFeaturesPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFiltersPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageSegmentationPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.MenuBarObserver;
import ru.nsu.ccfit.golovin.lab1.util.FilterUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Slf4j
public class ImageFiltersPanel extends JPanel implements ImageFiltersPanelObservable, ImageFeaturesPanelObserver,
        ImageSegmentationPanelObserver, MenuBarObserver {
    private static final List<ImageFiltersPanelObserver> observers = new ArrayList<>();
    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);

    private final AtomicInteger gridY = new AtomicInteger(0);
    private JSlider sigmaSlider;
    private JSlider thetaSlider;

    private BufferedImage image;

    public ImageFiltersPanel() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder("Image Filters Panel"));

        addFilterButtons();
        addSigmaSlider();
        addThetaSlider();
    }

    private void addFilterButtons() {
        Stream.of(Filter.values()).forEach(filter -> {
            final JButton filterButton = new JButton(filter.toString());
            filterButton.addActionListener(e -> {
                if (image == null) {
                    return;
                }
                switch (Filter.fromString(e.getActionCommand())
                        .orElseThrow(() -> new AssertionError("Unknown filter " + e.getActionCommand()))) {
                    case GAUSSIAN_BLUR:
                        image = FilterUtils.gaussianBlur(image, sigmaSlider.getValue());
                        break;
                    case SOBEL_OPERATOR:
                        image = FilterUtils.sobelOperator(image);
                        break;
                    case GABOR_FILTER:
                        image = FilterUtils.gaborFilter(image, thetaSlider.getValue() * 45);
                        break;
                }
                notifyAboutFilterApplying(image);
            });

            final GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridx = 0;
            constraints.gridy = gridY.getAndIncrement();
            constraints.weightx = 1.;
            constraints.insets = DEFAULT_INSETS;

            add(filterButton, constraints);
        });
    }

    private void addSigmaSlider() {
        sigmaSlider = new JSlider(SwingConstants.HORIZONTAL, 1, 10, 1);
        sigmaSlider.setPaintLabels(true);
        sigmaSlider.setMinorTickSpacing(1);
        sigmaSlider.setLabelTable(new Hashtable<>() {{
            put(1, new JLabel("1"));
            put(5, new JLabel("5"));
            put(10, new JLabel("10"));
        }});
        sigmaSlider.setPaintTicks(true);
        sigmaSlider.setBorder(BorderFactory.createTitledBorder("Sigma"));

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = gridY.getAndIncrement();
        constraints.weightx = 1.;
        constraints.insets = DEFAULT_INSETS;

        add(sigmaSlider, constraints);
    }

    private void addThetaSlider() {
        thetaSlider = new JSlider(SwingConstants.HORIZONTAL, 0, 3, 0);
        thetaSlider.setPaintLabels(true);
        thetaSlider.setLabelTable(new Hashtable<>() {{
            put(0, new JLabel("0"));
            put(1, new JLabel("45"));
            put(2, new JLabel("90"));
            put(3, new JLabel("135"));
        }});
        thetaSlider.setMinorTickSpacing(1);
        thetaSlider.setPaintTicks(true);
        thetaSlider.setBorder(BorderFactory.createTitledBorder("Theta"));

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = gridY.getAndIncrement();
        constraints.weightx = 1.;
        constraints.insets = DEFAULT_INSETS;

        add(thetaSlider, constraints);
    }

    @Override
    @SneakyThrows
    public void handleFileOpening(final File file) {
        changeImage(ImageIO.read(file));
    }

    private void reset() {
        image = null;
    }

    @Override
    public List<ImageFiltersPanelObserver> getObservers() {
        return observers;
    }

    @Override
    public void notifyAboutFilterApplying(final BufferedImage image) {
        observers.forEach(o -> o.handleFilterApplying(image));
    }

    @Override
    public void handleHsvChanging(final BufferedImage image) {
        changeImage(image);
    }

    private void changeImage(final BufferedImage image) {
        reset();
        this.image = image;
    }

    @Override
    public void handleImageSegmentation(final BufferedImage image) {
        changeImage(image);
    }
}
