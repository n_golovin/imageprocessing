package ru.nsu.ccfit.golovin.lab1.ui.observable;

import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageSegmentationPanelObserver;

import java.awt.image.BufferedImage;

public interface ImageSegmentationPanelObservable extends UIObservable<ImageSegmentationPanelObserver> {
    void notifyAboutImageSegmentation(BufferedImage image);
}
