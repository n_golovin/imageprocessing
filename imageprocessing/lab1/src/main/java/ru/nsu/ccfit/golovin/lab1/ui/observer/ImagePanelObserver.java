package ru.nsu.ccfit.golovin.lab1.ui.observer;

public interface ImagePanelObserver {
    void handleMouseClicking(int x, int y);

    void handleAreaSelection(int fromX, int fromY, int toX, int toY);
}
