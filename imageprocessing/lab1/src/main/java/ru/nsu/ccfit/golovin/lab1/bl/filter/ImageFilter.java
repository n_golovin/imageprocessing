package ru.nsu.ccfit.golovin.lab1.bl.filter;

import java.awt.image.BufferedImage;

public interface ImageFilter {
    BufferedImage applyFilter(BufferedImage image);
}
