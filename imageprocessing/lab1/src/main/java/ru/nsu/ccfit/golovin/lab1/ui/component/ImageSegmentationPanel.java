package ru.nsu.ccfit.golovin.lab1.ui.component;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.ccfit.golovin.lab1.domain.segmentation.Segmentation;
import ru.nsu.ccfit.golovin.lab1.ui.observable.ImageSegmentationPanelObservable;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFeaturesPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageFiltersPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.ImageSegmentationPanelObserver;
import ru.nsu.ccfit.golovin.lab1.ui.observer.MenuBarObserver;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Slf4j
public class ImageSegmentationPanel extends JPanel implements ImageSegmentationPanelObservable, ImageFeaturesPanelObserver,
        ImageFiltersPanelObserver, MenuBarObserver {
    private static final List<ImageSegmentationPanelObserver> observers = new ArrayList<>();
    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);

    private final AtomicInteger gridY = new AtomicInteger(0);
    private BufferedImage image;

    public ImageSegmentationPanel() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder("Image Segmentation Panel"));

        addSegmentationButtons();
    }

    private void addSegmentationButtons() {
        Stream.of(Segmentation.values()).forEach(segmentation -> {
            final JButton segmentationButton = new JButton(segmentation.toString());
            segmentationButton.addActionListener(e -> {
                if (image == null) {
                    return;
                }
                image = segmentation.applySegmentation(image);
                notifyAboutImageSegmentation(image);
            });


            final GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridx = 0;
            constraints.gridy = gridY.getAndIncrement();
            constraints.weightx = 1.;
            constraints.insets = DEFAULT_INSETS;

            add(segmentationButton, constraints);
        });
    }

    @Override
    @SneakyThrows
    public void handleFileOpening(final File file) {
        changeImage(ImageIO.read(file));
    }

    private void reset() {
        image = null;
    }

    private void changeImage(final BufferedImage image) {
        reset();
        this.image = image;
    }

    @Override
    public void notifyAboutImageSegmentation(final BufferedImage image) {
        observers.forEach(o -> o.handleImageSegmentation(image));
    }

    @Override
    public List<ImageSegmentationPanelObserver> getObservers() {
        return observers;
    }

    @Override
    public void handleHsvChanging(final BufferedImage image) {
        changeImage(image);
    }

    @Override
    public void handleFilterApplying(final BufferedImage image) {
        changeImage(image);
    }
}
