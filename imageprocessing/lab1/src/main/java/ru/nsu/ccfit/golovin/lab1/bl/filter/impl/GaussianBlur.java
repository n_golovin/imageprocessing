package ru.nsu.ccfit.golovin.lab1.bl.filter.impl;

import static java.lang.StrictMath.*;

public class GaussianBlur extends AbstractFilter {
    private static final int KERNEL_SIZE = 5;
    private final int sigma;

    public GaussianBlur(final int sigma) {
        this.sigma = sigma;
    }

    @Override
    double[][] getKernel() {
        final double[][] kernel = new double[KERNEL_SIZE][KERNEL_SIZE];
        final int mean = KERNEL_SIZE / 2;
        double sum = 0.;
        for (int i = 0; i < KERNEL_SIZE; i++) {
            for (int j = 0; j < KERNEL_SIZE; j++) {
                kernel[i][j] = exp(-0.5 * (pow((i - mean) / sigma, 2) + pow((j - mean) / sigma, 2))) /
                        (2 * PI * sigma * sigma);
                sum += kernel[i][j];
            }
        }
        for (int i = 0; i < KERNEL_SIZE; i++) {
            for (int j = 0; j < KERNEL_SIZE; j++) {
                kernel[i][j] /= sum;
            }
        }
        return kernel;
    }
}
