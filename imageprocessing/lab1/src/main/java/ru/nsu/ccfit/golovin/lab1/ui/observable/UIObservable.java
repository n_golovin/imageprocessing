package ru.nsu.ccfit.golovin.lab1.ui.observable;

import java.util.List;

public interface UIObservable<T> {
    default void addObserver(final T observer) {
        getObservers().add(observer);
    }

    List<T> getObservers();
}
