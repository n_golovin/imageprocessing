package ru.nsu.ccfit.golovin.lab1.ui.observable;

import ru.nsu.ccfit.golovin.lab1.ui.observer.MenuBarObserver;

import java.io.File;

public interface MenuBarObservable extends UIObservable<MenuBarObserver> {
    void notifyAboutFileOpening(File file);
}
