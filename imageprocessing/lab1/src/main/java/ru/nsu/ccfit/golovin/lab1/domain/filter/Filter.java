package ru.nsu.ccfit.golovin.lab1.domain.filter;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public enum Filter {
    GAUSSIAN_BLUR("Gaussian Blur"),
    SOBEL_OPERATOR("Sobel Operator"),
    GABOR_FILTER("Gabor Filter");

    private static final Map<String, Filter> stringToEnum = Stream.of(values()).collect(toMap(Object::toString, identity()));

    private final String prettyName;

    Filter(final String prettyName) {
        this.prettyName = prettyName;
    }

    @Override
    public String toString() {
        return prettyName;
    }

    public static Optional<Filter> fromString(final String string) {
        return Optional.ofNullable(stringToEnum.get(string));
    }
}
