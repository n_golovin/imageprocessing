package ru.nsu.ccfit.golovin.lab1.domain.segmentation;

import lombok.Data;

import java.awt.*;

@Data
public class ColorArea {
    private final Rectangle area;
    private Color color;
}
